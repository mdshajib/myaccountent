<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Income extends Model
{
    use Notifiable;

    protected $fillable = [
        'income',
    ];
}
