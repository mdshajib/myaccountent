<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Withdrowal extends Model
{
    protected $fillable = [
        'user_id', 'details', 'amount', 'type',
    ];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }
}
