<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Withdrowal;
use App\Balance;
use App\User;

class PartnerProfileController extends Controller
{
    public function index()
    {
    	$partners = User::where('role','partner')->where('status',1)->get();
    	return view('admin.home.withdrawal')->with('partners',$partners);
    }

    public function getWithdrawal()
    {
    	$partners = User::where('role','partner')->where('status',1)->get();
    	$withdraws = Withdrowal::with('user')->orderBy('created_at','desc')->paginate(10);
    	return view('admin.home.withdrawal',compact('partners','withdraws'));
    }

    public function withdrawal(Request $request)
    {
    	$this->validate($request,[

    		'partner'   => 'required',
            'details'   => 'required|string|max:100',
            'amount'    => 'required|numeric|between:0.01,9999999999.99',
            'paymode'   => 'required',
    	]);
    	$nowbalance = Balance::where('user_id',$request->partner)->first()->balance;
    	if($nowbalance >0)
    	{
    		if($request->amount > $nowbalance)
    		{
    			$error="Insuficient Balance. Balance is ".$nowbalance;
            	return back()->with('error',$error);
    		}
    		else
    		{
    			$withdraw = new Withdrowal;
		    	$withdraw->user_id = $request->partner;
		    	$withdraw->details = $request->details;
		    	$withdraw->amount = $request->amount;
		    	$withdraw->type = $request->paymode;

		    	if($withdraw->save())
		    	{
		    		if($this->BalanceUpdate($request->partner,$request->amount))
		    		{
		    			$success="Withdrawal was successfull.";
	                    return back()->with('success',$success);
		    		}
		    		else
		    		{
		    			$error="This amount was not deducted from Balance.";
	            		return back()->with('error',$error);
		    		}
		    	}
    		}
    		
    	}
    	else
    	{
    		$error="Balance is empty. Withdrawal failed.";
            return back()->with('error',$error);
    	}
    	
    	
    }

    private function BalanceUpdate($userid,$amount)
    {
    	$data = Balance::where('user_id',$userid)->first()->balance;
    	$balance = $data - $amount;
    	if(Balance::where('user_id',$userid)->update(['balance'=>$balance])){
    		return true;
    	}
    	else
    	{
    		return false;
    	}
    }

    public function getPartnerBalance($userid)
    {
    	$data = Balance::where('user_id',$userid)->first()->balance;
    	if($data > 0)
    	{
    		return $data;
    	}
    	else
    		return 0;
    }
}
