<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Transaction;
use App\Expensesheet;
use App\Incomesheet;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $role=Auth::user()->role;
        if($role =='admin')
        {
            $profit = 0.00;
            $partner = 0.00;
            $income = DB::table('incomes')->select('income')->first();
            $expense = DB::table('expenses')->select('expense')->first();

            if($income->income > $expense->expense)
            {
                $profit = $income->income - $expense->expense;

                if($profit>0)
                {
                    $partner = $profit/4;
                }
            }

            $incomesheets = Incomesheet::orderBy('created_at', 'desc')->take(5)->get();
            $expensesheets = Expensesheet::orderBy('created_at', 'desc')->take(5)->get();
            $transactions = Transaction::orderBy('created_at', 'desc')->take(3)->get();

            
            return view('admin.home.home')->with(compact('income','expense','profit','partner','incomesheets','expensesheets','transactions'));
        }
        else
        {
            return view('partner.home.dashboard');
        }
    }
}
