<?php

namespace App\Http\Controllers\POS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\POS\Brand;
use App\POS\Category;

class BrandController extends Controller
{
    public function index()
    {
    	$brands = Brand::all();
    	$categorys= Category::all();
    	return view('admin.pos.brand',compact('brands','categorys'));
    }

    public function store(Request $request)
    {
    	$this->validate($request,[
    		'brand'       		=> 'required|string|max:100',
    		'category'       	=> 'required',
    	]);

        $brand                  = ucfirst($request->brand);
        $category_id            = ucfirst($request->category);

        if(!Brand::where([
            'category_id' =>$category_id,
            'brand'       =>$brand
        ])->first())
        {
            $nbrand= new Brand;
            $nbrand->brand         = $brand;
            $nbrand->category_id   = $category_id;
        	if($nbrand->save())
        	{
        		$success = 'New Brand successfully added.';
        		return back()->with('success',$success);
        	}
        	else
        	{
        		$error = 'Brand Not added successfully !!!';
        		return back()->with('error',$error);
        	}
        }
        else
        {
            $error = 'Category and Brand already exist !!!';
            return back()->with('error',$error);
        }

    }
}
