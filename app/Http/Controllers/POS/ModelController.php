<?php

namespace App\Http\Controllers\POS;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\POS\Category;
use App\POS\Models;
use Input;

class ModelController extends Controller
{
    public function index()
    {
    	$categorys= Category::all();
    	$models= Models::with('brand','brand.category')->get();
    	return view('admin.pos.model',compact('categorys','models'));
    }

    public function store(Request $request)
    {
    	$this->validate($request,[
    		'brand'    => 'required',
    		'model'       => 'required|string|max:100',
    	]);

        $brand_id = $request->input('brand');
        $modelname = ucfirst($request->input('model'));
        if (!Models::where('brand_id',$brand_id)->where('model',$modelname)->first())
        {
        	$model = new Models;
        	$model->brand_id = $brand_id;
        	$model->model = $modelname;
        	if($model->save())
        	{
        		$success='New Model successfully added.';
        		return back()->with('success',$success);
        	}
        	else
        	{
        		$error='Model not added successfully !!!';
        		return back()->with('error',$error);
        	}
        }
        else
        {
            $error='Model already exist !!!';
            return back()->with('error',$error);
        }

    }
}
