<?php

namespace App\Http\Controllers\POS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\POS\Category;

class CategoryController extends Controller
{
    public function index()
    {
    	$categorys= Category::all();
    	return view('admin.pos.category')->with('categorys',$categorys);
    }

    public function store(Request $request)
    {
    	$this->validate($request,[
    		'category'       => 'required|string|max:100',
    	]);

        $ncategory = ucfirst($request->category);
        if (!Category::where('category',$ncategory)->first())
        {
        	$category = new Category;
        	$category->category = $ncategory;
        	if($category->save())
        	{
        		$success='New Category successfully added.';
        		return back()->with('success',$success);
        	}
        	else
        	{
        		$error='Category not added successfully !!!';
        		return back()->with('error',$error);
        	}
        }
        else
        {
            $error='Category already exist !!!';
            return back()->with('error',$error);
        }

    }
}
