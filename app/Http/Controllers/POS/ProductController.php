<?php

namespace App\Http\Controllers\POS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\POS\Product;
use App\POS\Category;
use App\POS\Brand;
use App\POS\Models;
use App\POS\Stock;

class ProductController extends Controller
{
    public function index()
    {
    	$products= Product::with('brand')->get();
    	$categories=Category::all();
    	return view('admin.pos.product',compact('products','categories'));
    }

    
    public function store(Request $request)
    {
    	$this->validate($request,[
    		'brand'       		=> 'required',
            'models'            => 'required',
    		'product'       	=> 'required|string|max:100',
    		'description'       => 'required|string|max:100',
            'origin'            => 'required|string|max:100',
            'currency'          => 'required',
    		'price'       		=> 'required|numeric|between:0.01,9999999999.99',
    	]);

    	$brand= $request->brand;
        $models= $request->models;
    	$nproduct= ucfirst($request->product);
    	if(!Product::where([
            'brand_id' =>$brand,
            'model_id' =>$models,
            'product'  =>$nproduct
        ])->first())
    	{
    		$product = new Product;
    		$product->brand_id 		=$brand; 
            $product->model_id      =$models; 
    		$product->product 		=$nproduct; 
    		$product->description 	=$request->description; 
            $product->origin        =ucfirst($request->origin); 
    		$product->currency 		=$request->currency; 
            $product->price         =$request->price; 

    		if($product->save())
    		{
    			$productid=$product->id;

    			$stock = new Stock;
    			$stock->product_id 	= $productid;
    			$stock->stock 		= 0;
    			if($stock->save())
    			{
	    			$success = 'New Product successfully added.';
	        		return back()->with('success',$success);
	        	}
	        	else
	        	{
	        		$error = 'Product added successfully, but not added in stock !!!';
            		return back()->with('error',$error);
	        	}
    		}
    		else
    		{
    			$error = 'Product not added successfully !!!';
            	return back()->with('error',$error);
    		}

    	}
    	else
        {
            $error = 'Brand and Product is already exist !!!';
            return back()->with('error',$error);
        }
    }

    public function getBrands($catid)
    {
        $brands= Brand::where('category_id',$catid)->get();
        foreach ($brands as $brand) {
            $output[$brand->id] = $brand->brand;
        }
        return $output;
    }

    public function getModels($brandid)
    {
        $models= Models::where('brand_id',$brandid)->get();
        foreach ($models as $model) {
            $output[$model->id] = $model->model;
        }
        return $output;
    }

    public function getProducts($brandid)
    {
        $products= Product::where('brand_id',$brandid)->get();
        foreach ($products as $product) {
            $output[$product->id] = $product->product;
        }
        return $output;
	}

    public function getPrice($productid)
    {
        $product= Product::findOrFail($productid);
        $data['price'] = $product->price;
        $data['currency'] = $product->currency;
        return $data;
    }
	
	public function stockList()
	{
		$stocks = Stock::with('product','product.brand','product.brand.category')->get();
		return view('admin.pos.stock')->with('stocks',$stocks);
	}

	public function getStock($productid)
	{
		$stocks = Stock::where('product_id',$productid)->first()->stock;
		return $stocks;
	}
}
