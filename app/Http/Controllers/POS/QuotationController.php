<?php

namespace App\Http\Controllers\POS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\POS\Category;
use App\POS\Brand;
use App\POS\Product;
use App\POS\Quotation;
use App\POS\Quotationdetails;
use App\POS\Customer;
use DB;

class QuotationController extends Controller
{
    public function index()
    {
        $categorys = Category::all();
        $customers = Customer::all();
        return view('admin.pos.quotation', compact('categorys','customers'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
    		'quotation'     => 'required|string|max:100',
    		'grandtotal'    => 'required|numeric|between:0.01,9999999999.99',
    		'customer'      => 'required|string',
    		'address'       => 'required|string|max:100',
    		'product'       => 'required',
        ]);

        $product=$request->product;
    	$unit=$request->unit;
    	$price=$request->price;

        $length=sizeof($product);
        
        $quot = new Quotation;
    	$quot->quotation		= $request->quotation;
        $quot->grandtotal	    = $request->grandtotal;
        $quot->customername	    = $request->customer;
        $quot->address	        = $request->address;

        if($quot->save())
    	{
    		$quotid= $quot->id;

    		for ($i=0; $i <$length ; $i++) { 
    			$quotdetails = new Quotationdetails;
    			$quotdetails->quotation_id  = $quotid;
    			$quotdetails->product_id 	= $product[$i];
    			$quotdetails->unit 		    = $unit[$i];
    			$quotdetails->unitprice	    = $price[$i];
    			$quotdetails->totalprice 	= bcmul($unit[$i],$price[$i],2);

                $quotdetails->save();
            }
            $success="Quotation added successfully.";
            return back()->with('success',$success);
    	}
    	else
    	{
    		$error="Fail to Quotation operation!!!";
            return back()->with('error',$error);
    	}       
    }

    public function allQuotation()
    {
        $quotations = Quotation::all();
        return view('admin.pos.allquotation')->with('quotations',$quotations);
    }

    public function quotationDetails($ids)
    {
        $id=decrypt($ids);

    	$details = Quotation::with(['quotDetails','quotDetails.product'])->where('id',$id)->first();
    	return view('admin.pos.quotationdetails')->with('details',$details);
    }
}
