<?php

namespace App\Http\Controllers\POS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\POS\Purchase;
use App\POS\Purchasedetails;
use App\POS\Category;
use App\POS\Product;
use App\POS\Stock;
use App\Expense;
use DB;
use Illuminate\Pagination\Paginator;

class PurchaseController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
    	$categorys = Category::all();
        return view('admin.pos.purchase')->with('categories',$categorys);
    }

    public function Purchase(Request $request)
    {
    	$this->validate($request,[
    		'invoice'      => 'required|string|max:100',
    		'grandtotal'   => 'required|numeric|between:0.01,9999999999.99',
    		'status'       => 'required|string',
    		'paymode'      => 'required|string|max:100',
    		'product'      => 'required',
        ]);
        
    	$product=$request->product;
    	$unit=$request->unit;
    	$price=$request->price;

    	$length=sizeof($product);

    	

    	$purchase = new Purchase;
    	$purchase->invoice		= $request->invoice;
    	$purchase->grandtotal	= $request->grandtotal;
    	$purchase->status 		= $request->status;
    	$purchase->paymode		= $request->paymode;

    	if($purchase->save())
    	{
    		$purchaseid= $purchase->id;

    		for ($i=0; $i <$length ; $i++) { 
    			$pdetails = new Purchasedetails;
    			$pdetails->purchase_id 	= $purchaseid;
    			$pdetails->product_id 	= $product[$i];
    			$pdetails->unit 		= $unit[$i];
    			$pdetails->unitprice	= $price[$i];
    			$pdetails->totalprice 	= bcmul($unit[$i],$price[$i],2);

                if($pdetails->save())
                {
                    $this->Stock($product[$i],$unit[$i]);
                }

    		}


    		if($request->status=='paid')
            {
                if($this->UpdateExpense($request->grandtotal))
                {
                    $success="Purchase added successfully.";
                    return back()->with('success',$success);
                }
                else
                {
                    $error="Purchase not update successfully!!!";
                    return back()->with('error',$error);
                }
            }
            else
            {
                $success="Purchase is unpaid.";
                return back()->with('success',$success);
            }
    	}
    	else
    	{
    		$error="Fail to purchase operation!!!";
            return back()->with('error',$error);
    	}


    	
    	
    }

    public function Stock($productid, $quantity)
    {
        $product = Stock::where('product_id', $productid)->first();
        $qt = $product->stock + $quantity;
        $product->stock = $qt;
        $product->save();
    }

    public function UnpaidPurchase()
    {
    	$duepurchases=Purchase::where('status','unpaid')->paginate(5);
    	return view('admin.pos.unpaidpurchase')->with('duepurchases',$duepurchases);
    }

    public function PaidPurchase($ids)
    {
    	$id=decrypt($ids);
        $purchase=Purchase::where('id',$id)->first();
        if($purchase !=null && $purchase->grandtotal>0)
        {
            if($this->UpdateExpense($purchase->grandtotal))
            {
                $purchase->status="paid";
                $purchase->save();

                $success="Due amount is paid successfully.";
                return back()->with('success',$success);
            }
            else
            {
                $error="Paid not successfull.";
               return back()->with('error',$error);
            }
        }
        else
            return back();
    }

    private function UpdateExpense($amount)
    {
        $expense = DB::table('expenses')->select('*')->first();
        $newexpense = $expense->expense+$amount;

        $update = Expense::find(1);
        $update->expense = $newexpense;
        if($update->save())
        {
            return true;
        }
        else
            return false;
    }

    public function PurchaseDetails($ids)
    {

    	$id=decrypt($ids);

    	$purchase = Purchase::where('id',$id)->first();
    	$details = Purchasedetails::with('product')->where('purchase_id',$id)->get();

    	return view('admin.pos.purchasedetails', compact('purchase','details'));
    	
    }
}
