<?php

namespace App\Http\Controllers\POS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\POS\Category;
use App\POS\Sale;
use App\POS\Saledetails;
use App\POS\Product;
use App\POS\Stock;
use App\Income;
use DB;
use Illuminate\Pagination\Paginator;


class SaleController extends Controller
{
    public function index()
    {
    	$categorys = Category::all();
    	return view('admin.pos.sale')->with('categories',$categorys);
    }

    public function Sale(Request $request)
    {
    	$this->validate($request,[
    		'invoice'      => 'required|string|max:100',
    		'grandtotal'   => 'required|numeric|between:0.01,9999999999.99',
            'status'       => 'required|string',
            'customer'     => 'required|string|max:100',
    		'address'      => 'required|string|max:100',
    		'paymode'      => 'required|string|max:100',
    		'product'      => 'required',

        ]);
        
    	$product=$request->product;
    	$unit=$request->unit;
    	$price=$request->price;

    	$length=sizeof($product);

    	

    	$sale = new Sale;
    	$sale->invoice		    = $request->invoice;
        $sale->grandtotal	    = $request->grandtotal;
        $sale->customername		= $request->customer;
    	$sale->address		    = $request->address;
    	$sale->status 		    = $request->status;
    	$sale->paymode		    = $request->paymode;


    	if($sale->save())
    	{
    		$saleid= $sale->id;

    		for ($i=0; $i <$length ; $i++) { 
    			$sdetails = new Saledetails;
    			$sdetails->sale_id  	= $saleid;
    			$sdetails->product_id 	= $product[$i];
    			$sdetails->unit 		= $unit[$i];
    			$sdetails->unitprice	= $price[$i];
    			$sdetails->totalprice 	= bcmul($unit[$i],$price[$i],2);

                if($sdetails->save())
                {
                    $this->Stock($product[$i],$unit[$i]);
                }

    		}


    		if($request->status=='paid')
            {
                if($this->UpdateIncome($request->grandtotal))
                {
                    $success="Sale added successfully.";
                    return back()->with('success',$success);
                }
                else
                {
                    $error="Sale not update successfully!!!";
                    return back()->with('error',$error);
                }
            }
            else
            {
                $success="Sale is unpaid.";
                return back()->with('success',$success);
            }
    	}
    	else
    	{
    		$error="Fail to Sale operation!!!";
            return back()->with('error',$error);
    	}
    }

    public function Stock($productid, $quantity)
    {
        $product = Stock::where('product_id', $productid)->first();
        $qt = $product->stock - $quantity;
        $product->stock = $qt;
        $product->save();
    }

    

    public function UnpaidSale()
    {
    	$duesales=Sale::where('status','unpaid')->paginate(5);
    	return view('admin.pos.unpaidsale')->with('duesales',$duesales);
    }

    public function PaidSale($ids)
    {
    	$id=decrypt($ids);
        $sale=Sale::where('id',$id)->first();
        if($sale !=null && $sale->grandtotal>0)
        {
            if($this->UpdateIncome($sale->grandtotal))
            {
                $sale->status="paid";
                $sale->save();

                $success="Due amount is paid successfully.";
                return back()->with('success',$success);
            }
            else
            {
                $error="Paid not successfull.";
               return back()->with('error',$error);
            }
        }
        else
            return back();
    }

    public function SaleDetails($ids)
    {

    	$id=decrypt($ids);

    	$sale = Sale::where('id',$id)->first();
    	$details = Saledetails::with('product')->where('sale_id',$id)->get();

    	return view('admin.pos.saledetails', compact('sale','details'));
    	
    }

    private function UpdateIncome($amount)
    {
        $income = DB::table('incomes')->select('*')->first();
        $newincome = $income->income+$amount;

        $update = Income::find(1);
        $update->income = $newincome;
        if($update->save())
        {
            return true;
        }
        else
            return false;
    }


}
