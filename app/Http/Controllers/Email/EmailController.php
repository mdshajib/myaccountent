<?php

namespace App\Http\Controllers\Email;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mail;
use App\Mail\SendSupport;
use Session;

class EmailController extends Controller
{
    public function index()
    {
    	return view('email.sendemail');
    }


    public function SendSupportTest(Request $request)
    {
        $sendto="shajib.net0@gmail.com";
        $subject="This is test";
        $message="Hello shajib. this is testing email.";

        Mail::to($sendto)->send(new SendSupport($subject,$message));
        Session::flash("Mail send successfully.");
        return "Mail send successfully.";
    }

    public function SendNewUserNotification()
    {
        return "Send";
    }
}
