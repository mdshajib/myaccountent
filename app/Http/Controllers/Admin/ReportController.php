<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;
use App\Employeesalary;
use App\Incomesheet;
use App\Expensesheet;
use PDF;

class ReportController extends Controller
{


    public function IncomeReportMonthly(Request $request)
    {
        $month = $request->month;
        $year = $request->year;
        $filename = "incomereport-".$month.$year;
        $incomes = Incomesheet::whereYear('created_at', $year)
                            ->whereMonth('created_at', $month)
                            ->orderBy('created_at', 'desc')
                            ->get();
        $pdf = PDF::loadView('admin.home.incomepdf',compact('incomes'))->setPaper('a4','portrait');  
        return $pdf->download($filename.'.pdf');
    }

    public function ExpenseReportMonthly(Request $request)
    {
        $month = $request->month;
        $year = $request->year;
        $filename = "expensereport-".$month.$year;
        $expenses = Expensesheet::whereYear('created_at', $year)
                            ->whereMonth('created_at', $month)
                            ->orderBy('created_at', 'desc')
                            ->get();
        $pdf = PDF::loadView('admin.home.expensepdf',compact('expenses'))->setPaper('a4','portrait');  
        return $pdf->download($filename);
    }


    public function report($report)
    {
        $report_type=$report;
        if($report=='incomesheet')
        {
            $incomes=Incomesheet::orderBy('created_at', 'desc')->paginate(10);
            return view('admin.home.incomesheet')->with('incomes',$incomes);
        }
        else if($report=='expensesheet')
        {
            $expenses=Expensesheet::orderBy('created_at', 'desc')->paginate(10);
            return view('admin.home.expensesheet')->with('expenses',$expenses);
        }
        else if($report=='commision')
        {
            $commissions=Incomesheet::where('incometype','Commision Base')->orderBy('created_at', 'desc')->paginate(10);
            return view('admin.home.report.commisionreport')->with('commissions',$commissions);
        }
        else if($report=='consultency')
        {
            $consultencys=Incomesheet::where('incometype','Consuntency Fee')->orderBy('created_at', 'desc')->paginate(10);
            return view('admin.home.report.consultencyreport')->with('consultencys',$consultencys);
        }
        else if($report=='officeaccesories')
        {
            $accesoriess=Expensesheet::where('expensestype','Office Accesories')->orderBy('created_at', 'desc')->paginate(10);
            return view('admin.home.report.officeaccesoriesreport')->with('accesoriess',$accesoriess);
        }
        else if($report=='officerent')
        {
            $rents=Expensesheet::where('expensestype','Office Rent')->orderBy('created_at', 'desc')->paginate(10);
            return view('admin.home.report.officerentreport')->with('rents',$rents);
        }
        else if($report=='customergift')
        {
            $gifts=Expensesheet::where('expensestype','Customer Gift')->orderBy('created_at', 'desc')->paginate(10);
            return view('admin.home.report.customergiftreport')->with('gifts',$gifts);
        }
        else if($report=='incomethismonth')
        {
            $month = date('m');
            $year = date('Y');
            $filename = "incomereport-".$month.$year;
            $incomes = Incomesheet::whereYear('created_at', $year)
                                ->whereMonth('created_at', $month)
                                ->orderBy('created_at', 'desc')
                                ->get();
            $pdf = PDF::loadView('admin.home.incomepdf',compact('incomes'))->setPaper('a4','portrait');  
            return $pdf->download($filename.'.pdf');
        }
        else if($report=='expensethismonth')
        {
            $month = date('m');
            $year = date('Y');
            $filename = "expensereport-".$month.$year;
            $expenses = Expensesheet::whereYear('created_at', $year)
                                ->whereMonth('created_at', $month)
                                ->orderBy('created_at', 'desc')
                                ->get();
            $pdf = PDF::loadView('admin.home.expensepdf',compact('expenses'))->setPaper('a4','portrait');  
            return $pdf->download($filename);
        }
        else if($report=='salary')
        {
            return "salary report";
        }
    }
}
