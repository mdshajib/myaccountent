<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Incomesheet;
use App\Supplyer;
use App\Income;
use DB;

class IncomeController extends Controller
{
    public function IncomeSheet()
    {
        $incomes = Incomesheet::orderBy('created_at', 'desc')->get();
        return view('admin.home.incomesheet')->with('incomes',$incomes);
    }
    public function CommisionForm()
    {
        $companys = Supplyer::select('name')->get();
        $unpaids = Incomesheet::where('status','unpaid')
                                ->where('incometype','Commision Base')
                                ->get();

    	return view('admin.home.commisionbase')->with(['companys'=>$companys,'unpaids'=>$unpaids]);
    }

    public function Commision(Request $request)
    {

        $this->validate($request,[
            'company'       => 'required|string|max:100',
            'address'       => 'required|string|max:100',
            'invoice'       => 'required|string|max:100',
            'amount'        => 'required|numeric|between:0.01,9999999999.99',
            'paymode'       => 'required',
            'status'        => 'required',
        ]);

        $income = new Incomesheet;
        $income->incometype     ='Commision Base';
        $income->company        =$request->company;
        $income->invoice        =$request->invoice;
        $income->amount         =$request->amount;
        $income->paymode        =$request->paymode;
        $income->status         =$request->status;


        if($income->save())
        {
            if($request->status=='paid')
            {
                if($this->UpdateIncome($request->amount))
                {
                    $success="Commision Base income successfull.";
                    return back()->with('success',$success);
                }
                else
                {
                    $error="Commision Base not update in income.";
                    return back()->with('error',$error);
                }
            }
            else
            {
                $success="Commision Base income is unpaid.";
                return back()->with('success',$success);
            }
        }
        else
            {
               $error="Fail to store data in this system.";
               return back()->with('error',$error);
            }
    }

    public function ConsultencyForm()
    {
        $unpaids = Incomesheet::where('status','unpaid')
                                ->where('incometype','Consuntency Fee')
                                ->get();

    	return view('admin.home.consultency')->with('unpaids',$unpaids);
    }

    public function Consultency(Request $request)
    {

    	$this->validate($request,[
            'company'       => 'required|string|max:100',
            'address'       => 'required|string|max:100',
            'invoice'       => 'required|string|max:100',
            'amount'        => 'required|numeric|between:0.01,9999999999.99',
            'paymode'       => 'required',
            'status'        => 'required',
        ]);

        $income = new Incomesheet;
        $income->incometype     ='Consuntency Fee';
        $income->company        =$request->company;
        $income->invoice        =$request->invoice;
        $income->amount         =$request->amount;
        $income->paymode        =$request->paymode;
        $income->status         =$request->status;

        if($income->save())
        {
            if($request->status=='paid')
            {
                if($this->UpdateIncome($request->amount))
                {
                    $success="Consultency Fee income successfull.";
                    return back()->with('success',$success);
                }
                else
                {
                    $error="Consultency fee not update in income.";
                    return back()->with('error',$error);
                }
            }
            else
            {
                $success="Consultency Fee is unpaid.";
                return back()->with('success',$success);
            }
            
        }
        else
            {
               $error="Fail to store data in this system.";
               return back()->with('error',$error);
            }
    }

    public function PaidIncomes($ids)
    {
        $id=decrypt($ids);
        $amount=Incomesheet::select('amount')->where('id',$id)->first();
        if($amount !=null && $amount->amount>0)
        {
            if($this->UpdateIncome($amount->amount))
            {
                $update = Incomesheet::find($id);
                $update->status="paid";
                $update->save();

                $success="Due amount is paid successfully.";
                return back()->with('success',$success);
            }
            else
            {
                $error="Paid not successfull.";
               return back()->with('error',$error);
            }
        }
        else
            return back();
        
        
    }


    private function UpdateIncome($amount)
    {
        $income = DB::table('incomes')->select('*')->first();
        $newincome = $income->income+$amount;

        $update = Income::find(1);
        $update->income = $newincome;
        if($update->save())
        {
            return true;
        }
        else
            return false;
    }

    
}
