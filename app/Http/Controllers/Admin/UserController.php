<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\User;
use Input;
use App\EmailClass\EmailSender;


class UserController extends Controller
{
    public function index()
    {
        $users=User::all();
    	return view('admin.home.users')->with('users',$users);
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'fullname'   => 'required|string|max:100',
            'email'      => 'required|string|email|max:100|unique:users,email',
            'phone'      => 'required|string|max:20',
            'password'   => 'required|max:30',
            'role'       => 'required',
        ]);

        $useremail=$request->input('email');
        $email=User::where('email',$useremail)->first();
        $phone=User::where('phone',$request->input('phone'))->first();
        if($email)
        {
            $error="This Email already exist in this system !!.";
            return back()->with('error',$error);
        }
        else if($phone)
        {
            $error="This Phone already exist in this system !!.";
            return back()->with('error',$error);
        }
        else
        {
            $user = new User;
            $user->name = $request->input('fullname');
            $user->email = $useremail;
            $user->phone = $request->input('phone');
            $user->password =Hash::make($request->input('password'));
            $user->role = $request->input('role');
            $user->status = $request->input('status') ? $request->input('status') :0;

            if($user->save())
            {
                $message="Hello, Mr. ".$request->fullname.". Your account successfully created. You will get an activation link when try to log in.";
                //$emailsend = EmailSender::SendNotice($useremail,"New user create",$message);
                //$success="New user successfully created. ".$emailsend;
                $success="New user successfully created. ";
                return back()->with('success',$success); 
            }
            else
            {
                $error="Fail to create new user !!.";            
                return back()->with('error',$error);
            }

        } 
        
    }
}
