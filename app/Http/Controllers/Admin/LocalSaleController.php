<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Localsale;
use App\Incomesheet;
use App\Income;
use DB;

class LocalSaleController extends Controller
{
    public function LocalSaleForm()
    {

    	return view('admin.home.localsale');
    }

    public function LocalSale(Request $request)
    {

    	$this->validate($request,[
            'company'       => 'required|string|max:100',
            'invoiceno'     => 'required|string|max:100',
            'product'       => 'required|string|max:100',
            'price'        	=> 'required|numeric|between:0.01,9999999999.99',
            'quantity'      => 'required|integer|min:1',
            'totalprice'    => 'required|numeric|between:0.01,9999999999.99',
            'paymenttype'   => 'required|string|max:10',
        ]);

        $sale = new Localsale;
        $sale->company      =$request->company;
        $sale->invoiceno    =$request->invoiceno;
        $sale->product     	=$request->product;
        $sale->price 		=$request->price;
        $sale->quantity    	=$request->quantity;
        $sale->totalprice   =$request->totalprice;
        $sale->paymenttype 	=$request->paymenttype;

        if($sale->save())
        {
            if($this->UpdateIncome($request->totalprice))
            {
                $incomesheet = new Incomesheet;
                $incomesheet->incometype   = 'Local Sale';
                $incomesheet->company      = $request->company;
                $incomesheet->invoiceno    = $request->invoiceno;
                $incomesheet->amount       = $request->totalprice;
                $incomesheet->paymenttype  = $request->paymenttype;
                $incomesheet->save();

                $success="Successfully Local Sale.";
                return back()->with('success',$success);
            }
            else
            {
                $error="Local sale not update in income.";
                return back()->with('error',$error);
            }
        }
        else
            {
               $error="Fail to Local Sale.";
               return back()->with('error',$error);
            }
    }

    private function UpdateIncome($amount)
    {
        $income = DB::table('incomes')->select('*')->first();
        $newincome = $income->income+$amount;

        $update = Income::find(1);
        $update->income = $newincome;
        if($update->save())
        {
            return true;
        }
        else
            return false;
    }
}
