<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Expense;
use App\Income;
use App\Profit;
use App\Transaction;
use App\Partner;
use App\User;

class CloseCalculationController extends Controller
{
    public function CloseMonthlyCal()
    {
    	if(Auth::check())
    	{
    		$income 	=	Income::select('income')->first();
    		$expense 	=	Expense::select('expense')->first();
    		$profit 	=	0.00;

    		if($income->income > $expense->expense)
    		{
    			$profit = $income->income - $expense->expense;
    			if($profit>0)
    			{
    				$this->SetProfit($profit);
    				$txid = $this->SetTransaction($income->income,$expense->expense,$profit);
    				if($txid !=0)
    				{
    					if($this->SetPartnerProfit($txid,$profit))
    					{
    						if($this->ClearExpenseIncomeTable())
    						{
    							$success="Successfully closed monthly calculation.";
    							return view('admin.home.closemonthly')->with(compact('success'));
    						}
    						else
    						{
    							$error="Previous income and expense not cleared.";
    							return view('admin.home.closemonthly')->with(compact('error'));
    						}
    					}
    					else
    						{
    							$error="Partner profit not set in partner table.";
    							return view('admin.home.closemonthly')->with(compact('error'));
    						}
    				}
    				else
						{
							$error="Transaction not successfully done.";
							return view('admin.home.closemonthly')->with(compact('error'));
						}
    			}
    			
    		}
    		else if($income->income == $expense->expense)
    		{
    			$profit = $income->income - $expense->expense;
    			if($profit>=0)
    			{
    				$this->SetProfit($profit);
    				$txid = $this->SetTransaction($income->income,$expense->expense,$profit);
    				if($txid !=0)
    				{
    					if($this->SetPartnerProfit($txid,$profit))
    					{
    						if($this->ClearExpenseIncomeTable())
    						{
    							$success="Successfully closed monthly calculation but not profit.";
    							return view('admin.home.closemonthly')->with(compact('success'));
    						}
    						else
    						{
    							$error="Previous income and xxpense not cleared.";
    							return view('admin.home.closemonthly')->with(compact('error'));
    						}
    					}
    					else
    						{
    							$error="Partner profit not set in partner table.";
    							return view('admin.home.closemonthly')->with(compact('error'));
    						}
    				}
    				else
						{
							$error="Transaction not successfully done.";
							return view('admin.home.closemonthly')->with(compact('error'));
						}
    			}
    		}

    		else
    		{
    			$error="The company was damaged this month. Expense more than income. Not closed.";
				return view('admin.home.closemonthly')->with(compact('error'));
    		}

    	}
    }

    private function SetProfit($profit)
    {
    	$setprofit = new Profit;
		$setprofit->profit = $profit;
		if($setprofit->save())
			return true;
		else
			return false;
    }

    private function SetTransaction($income,$expense,$profit)
    {
    	$trxid = rand(49,99).time();
    	if($profit>0)
    	{
    		$each  = $profit/4;
    	}
    	else
    	{
    		$each=0.00;
    	}

    	$transaction = new Transaction;

    	$transaction->trxid 		= $trxid;
    	$transaction->income 		= $income;
    	$transaction->expense 		= $expense;
    	$transaction->profit 		= $profit;
    	$transaction->eachpartner	= $each;

    	if($transaction->save())
    		return $trxid;
    	else
    		return 0;
    }

    private function SetPartnerProfit($trxid,$profit)
    {
    	$partnersid = User::where('role','partner')->select('userid')->get();
    	if($profit >0)
    	{
    		$myprofit  = $profit/4;
    	}
    	else
    	{
    		$myprofit = 0.00;
    	}
    	

    	if($partnersid !=null)
    	{
    		foreach ($partnersid as $partnerid) {
    			$partner 				= new Partner;
    			$partner->user_id 		= $partnerid->userid;
    			$partner->trxid 		= $trxid;
    			$partner->companyprofit = $profit;
    			$partner->myprofit 		= $myprofit;
    			$partner->save();
    		}
    		return true;
    	}
    	else
    		return false;
    }

    private function ClearExpenseIncomeTable()
    {
    	$expense = Expense::find(1);
    	$income  = Income::find(1);
    	$expense->expense = 0.00;
    	$income->income = 0.00;

    	if($expense->save() && $income->save())
    		return true;
    	else
    		return false;
    }
}
