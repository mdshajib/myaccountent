<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Supplyer;

class SupplyerController extends Controller
{
	public function index()
    {
        $companys = Supplyer::all();

    	return view('admin.home.supplyer')->with('companys',$companys);
    }

    public function store(Request $request)
    {
        
        $this->validate($request,[
            'company'   => 'required|string|max:100',
            'email'     => 'required|string|max:100',
            'phone'     => 'required|string|max:20',
            'address'   => 'required|string|max:100',
            'website'   => 'required|string|max:20',
        ]);

        $comemail=$request->companyemail;
        $email=Supplyer::where('email',$comemail)->first();
        if(!$email)
        {
            $company= new Supplyer;

            $company->name      = $request->company;
            $company->email     = $request->email;
            $company->website   = $request->website;
            $company->contacts  = $request->phone;
            $company->address   = $request->address;
            
            if($company->save())
                {
                    $success="New company successfully created.";
                    return back()->with('success',$success); 
                }
            else
            {
               $error="Fail to create new company !!.";
               return back()->with('error',$error);
            }
        }
        else
        {
            $error="This Email already exist in this system !!.";
            return back()->with('error',$error);
        }
            
    }
}
