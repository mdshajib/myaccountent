<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Employee;
use App\Employeesalary;

class EmployeeController extends Controller
{

    public function EmployeeSalaryForm(Request $request)
    {

    	return view('admin.home.employeesalary');
    }

    public function EmployeeSalary()
    {

    	return " ";
    }

    public function index()
    {
        $employes = Employee::all();

    	return view('admin.home.employee')->with('employes',$employes);
    }

    public function store(Request $request)
    {

        $this->validate($request,[
            'fullname'     => 'required|string|max:100',
            'designation'  => 'required|string|max:100',
            'email'        => 'required|string|max:100',
            'phone'        => 'required|string|max:20',
            'education'    => 'required|string|max:100',
            'salary'       => 'required|string|max:20',
            'date'         => 'required|string|max:20',
        ]);
        $empemail=$request->email;
        $email=Employee::where('email',$empemail)->first();
        if(!$email)
        {
            $employee= new Employee;

            $employee->name        = $request->fullname;
            $employee->designation = $request->designation;
            $employee->email       = $empemail;
            $employee->phone       = $request->phone;
            $employee->education   = $request->education;
            $employee->salary      = $request->salary;
            $employee->joiningdate = $request->date;
            
            if($employee->save())
                {
                    $success="New employee successfully created.";
                    return back()->with('success',$success); 
                }
            else
            {
               $error="Fail to create new employee !!.";
               return back()->with('error',$error);
            }
        }
        else
        {
            $error="This Email already exist in this system !!.";
            return back()->with('error',$error);
        }

    }
    

}
