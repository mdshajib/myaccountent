<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Expensesheet;
use App\Expense;
use DB;

class ExpenseController extends Controller
{
    
    public function ExpenseSheet()
    {
        $expenses=Expensesheet::orderBy('created_at', 'desc')->get();

        return view('admin.home.expensesheet')->with('expenses',$expenses);
    }

    public function OfficeAccesoriesForm()
    {
        $unpaids = Expensesheet::where('status','unpaid')
                                ->where('expensestype','Office Accesories')
                                ->get();

    	return view('admin.home.officeaccesories')->with('unpaids',$unpaids);
    }

    public function OfficeAccesories(Request $request)
    {

        $this->validate($request,[
            'itemname'      =>'required|string|max:100',
            'amount'        =>'required|numeric|between:0.01,9999999999.99',
            'paymode'       =>'required',
            'status'        =>'required',
            'paymode'       =>'required',
        ]);

        $accesories = new Expensesheet;

        $accesories->expensestype   = 'Office Accesories';
        $accesories->details        = $request->itemname;
        $accesories->invoice        = ' ';
        $accesories->amount         = $request->amount;
        $accesories->paymode        = $request->paymode;
        $accesories->status         = $request->status;

        if($accesories->save())
        {
            if($request->status=='paid')
            {
                if($this->UpdateExpense($request->amount))
                {
                    $success="Office Accesories successfully insert.";
                    return back()->with('success',$success);
                }
                else
                {
                    $error="Office accesories not update in expense.";
                    return back()->with('error',$error);
                }
            }
            else
            {
                $success="Office Accesories is unpaid.";
                return back()->with('success',$success);
            }
            
        }
        else
            {
               $error="Fail to insert Office Accesories.";
               return back()->with('error',$error);
            }
    }

    public function OfficeRentForm()
    {
        $unpaids = Expensesheet::where('status','unpaid')
                                ->where('expensestype','Office Rent')
                                ->get();
    	return view('admin.home.officerent')->with('unpaids',$unpaids);
    }

    public function OfficeRent(Request $request)
    {

    	$this->validate($request,[
            'rentdate'      =>'required|string|max:100',
            'amount'        =>'required|numeric|between:0.01,9999999999.99',
            'paymode'       =>'required',
            'status'        =>'required',
        ]);

        $accesories = new Expensesheet;

        $accesories->expensestype   = 'Office Rent';
        $accesories->details        = $request->rentdate;
        $accesories->invoice        = ' ';
        $accesories->amount         = $request->amount;
        $accesories->paymode        = $request->paymode;
        $accesories->status         = $request->status;

        if($accesories->save())
        {
            if($request->status=='paid')
            {
                if($this->UpdateExpense($request->amount))
                {
                    $success="Office Rent successfully insert.";
                    return back()->with('success',$success);
                }
                else
                {
                    $error="Office rent not update in expense.";
                    return back()->with('error',$error);
                }
            }
            else
            {
                $success="Office Rent is unpaid.";
                return back()->with('success',$success);
            }
        }
        else
            {
               $error="Fail to insert Office Rent.";
               return back()->with('error',$error);
            }
    }

    public function CustomerGiftForm()
    {
        $unpaids = Expensesheet::where('status','unpaid')
                                ->where('expensestype','Customer Gift')
                                ->get();
    	return view('admin.home.customergift')->with('unpaids',$unpaids);;
    }

    public function CustomerGift(Request $request)
    {

    	$this->validate($request,[
            'giftto'        =>'required|string|max:100',
            'amount'        =>'required|numeric|between:0.01,9999999999.99',
            'paymode'       =>'required',
            'status'        =>'required',
        ]);

        $accesories = new Expensesheet;

        $accesories->expensestype   = 'Customer Gift';
        $accesories->details        = 'Gift To '.$request->giftto;
        $accesories->invoice        = ' ';
        $accesories->amount         = $request->amount;
        $accesories->paymode        = $request->paymode;
        $accesories->status         = $request->status;

        if($accesories->save())
        {
            if($request->status=='paid')
            {
                if($this->UpdateExpense($request->amount))
                {
                    $success="Customer Gift successfully insert.";
                    return back()->with('success',$success);
                }
                else
                {
                    $error="Customer gift not update in expense.";
                    return back()->with('error',$error);
                }
            }
            else
            {
                $success="Customer Gift is unpaid.";
                return back()->with('success',$success);
            }
        }
        else
            {
               $error="Fail to insert Customer Gift.";
               return back()->with('error',$error);
            }
    }

    public function PaidExpenses($ids)
    {
        $id=decrypt($ids);
        $amount=Expensesheet::select('amount')->where('id',$id)->first();
        if($amount !=null && $amount->amount>0)
        {
            if($this->UpdateExpense($amount->amount))
            {
                $update = Expensesheet::find($id);
                $update->status="paid";
                $update->save();

                $success="Due amount is paid successfully.";
                return back()->with('success',$success);
            }
            else
            {
                $error="Paid not successfull.";
               return back()->with('error',$error);
            }
        }
        else
            return back();
        
        
    }

    private function UpdateExpense($amount)
    {
        $expense = DB::table('expenses')->select('*')->first();
        $newexpense = $expense->expense+$amount;

        $update = Expense::find(1);
        $update->expense = $newexpense;
        if($update->save())
        {
            return true;
        }
        else
            return false;
    }
}
