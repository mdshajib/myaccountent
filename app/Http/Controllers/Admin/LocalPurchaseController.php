<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Localpurchase;
use App\Expensesheet;
use App\Expense;
use DB;
use Auth;

class LocalPurchaseController extends Controller
{
    public function LocalPurchaseForm()
    {

    	return view('admin.home.localpurchase');
    }

    public function LocalPurchase(Request $request)
    {

    	$this->validate($request,[
            'company'       => 'required|string|max:100',
            'invoiceno'     => 'required|string|max:100',
            'product'       => 'required|string|max:100',
            'price'        	=> 'required|numeric|between:0.01,9999999999.99',
            'quantity'      => 'required|integer|min:1',
            'totalprice'    => 'required|numeric|between:0.01,9999999999.99',
            'paymenttype'   => 'required|string|max:10',
        ]);

        $purchase = new Localpurchase;
        $purchase->company      =$request->company;
        $purchase->invoiceno    =$request->invoiceno;
        $purchase->product     	=$request->product;
        $purchase->price 		=$request->price;
        $purchase->quantity    	=$request->quantity;
        $purchase->totalprice   =$request->totalprice;
        $purchase->paymenttype 	=$request->paymenttype;

        if($purchase->save())
        {
            if($this->UpdateExpense($request->totalprice))
            {
                $expensesheet = new Expensesheet;
                $expensesheet->expensestype = 'Local Purchase';
                $expensesheet->details      = $request->product;
                $expensesheet->invoiceno    = $request->invoiceno;
                $expensesheet->amount       = $request->totalprice;
                $expensesheet->paymenttype  = $request->paymenttype;
                $expensesheet->save();

                $success="Successfully Local Purchase.";
                return back()->with('success',$success);
            }
            else
            {
                $error="Local purchase not update in expense.";
                return back()->with('error',$error);
            }
            
        }
        else
            {
               $error="Fail to insert Local Purchase.";
               return back()->with('error',$error);
            }
    }

    private function UpdateExpense($amount)
    {
        $expense = DB::table('expenses')->select('*')->first();
        $newexpense = $expense->expense+$amount;

        $update = Expense::find(1);
        $update->expense = $newexpense;
        if($update->save())
        {
            return true;
        }
        else
            return false;
    }
}
