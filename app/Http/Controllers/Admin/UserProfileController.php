<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;

class UserProfileController extends Controller
{
    public function index()
    {
    	if(Auth::check())
        {
            $email = Auth::user()->email;
            $profile = User::where('email',$email)->first();
            return view('admin.home.profile')->with('profile',$profile);
        }
        else
            return redirect('login');
    		
    }

    public function ChangePassword()
    {
    	return "Change password";
    }
}
