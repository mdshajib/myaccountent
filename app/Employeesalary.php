<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Employeesalary extends Model
{
    use Notifiable;

    protected $fillable = [
        'employee_id','salary', 'status',
    ];
}
