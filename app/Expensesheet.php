<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Expensesheet extends Model
{
    use Notifiable;

    protected $fillable = [
        'expensestype','details','invoice','amount','status', 'paymode',
    ];
}
