<?php

namespace App\POS;

use Illuminate\Database\Eloquent\Model;

class Quotationdetails extends Model
{
    protected $fillable = [
        'quotation_id','product_id','unit','unitprice','totalprice',
    ];

    public function product()
    {
    	return $this->belongsTo('App\POS\Product','product_id');
    }

    public function quotation()
    {
        return $this->belongsTo('App\POS\Quotation');
    }
}
