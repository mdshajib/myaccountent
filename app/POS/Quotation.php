<?php

namespace App\POS;

use Illuminate\Database\Eloquent\Model;

class Quotation extends Model
{
    protected $fillable = [
        'quotation','grandtotal','customername','address',
    ];

    public function quotDetails()
    {
        return $this->hasMany('App\POS\Quotationdetails');
    }
}
