<?php

namespace App\POS;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    protected $fillable = [
        'product_id','stock',
    ];

    
    public function product()
    {
        return $this->belongsTo('App\POS\Product');
    }

    

}
