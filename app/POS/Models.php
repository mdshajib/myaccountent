<?php

namespace App\POS;

use Illuminate\Database\Eloquent\Model;

class Models extends Model
{
    protected $table="models";
    protected $fillable = [
        'brand_id','model',
    ];

    public function brand()
    {
    	return $this->belongsTo('App\POS\Brand');
    }

    public function product()
    {
    	return $this->hasMany('App\POS\Product');
    }
}
