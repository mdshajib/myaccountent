<?php

namespace App\POS;

use Illuminate\Database\Eloquent\Model;

class Saledetails extends Model
{
    protected $fillable = [
        'sale_id','product_id','unit','unitprice','totalprice',
    ];

    public function product()
    {
    	return $this->belongsTo('App\POS\Product','product_id');
    }
}
