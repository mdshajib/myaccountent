<?php

namespace App\POS;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'brand_id','model_id','product','origin','description','currency','price',
    ];

    public function brand()
    {
    	return $this->belongsTo('App\POS\Brand');
    }
    public function models()
    {
        return $this->belongsTo('App\POS\Models','model_id');
    }

    public function purchase()
    {
    	return $this->hasMany('App\POS\Purchasedetails');
    }

    public function stock()
    {
    	return $this->hasOne('App\POS\Stock');
    }
}
