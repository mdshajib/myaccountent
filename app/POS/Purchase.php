<?php

namespace App\POS;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    protected $fillable = [
        'invoice','grandtotal','status','paymode',
    ];
}
