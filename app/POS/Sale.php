<?php

namespace App\POS;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    protected $fillable = [
        'invoice','grandtotal','customername','address','status','paymode',
    ];
}
