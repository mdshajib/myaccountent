<?php

namespace App\POS;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $fillable = [
        'category_id','brand',
    ];

    public function category()
    {
    	return $this->belongsTo('App\POS\Category','category_id');
    }

    public function product()
    {
    	return $this->hasMany('App\POS\Product');
    }

     public function models()
    {
        return $this->hasMany('App\POS\Models');
    }
}
