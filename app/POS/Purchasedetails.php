<?php

namespace App\POS;

use Illuminate\Database\Eloquent\Model;

class Purchasedetails extends Model
{
    protected $fillable = [
        'purchase_id','product_id','unit','unitprice','totalprice',
    ];

    public function product()
    {
    	return $this->belongsTo('App\POS\Product','product_id');
    }
}
