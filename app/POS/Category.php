<?php

namespace App\POS;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'category'
    ];

    public function brand()
    {
    	return $this->hasMany('App\POS\Brand');
    }
}
