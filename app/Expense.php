<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Expense extends Model
{
    use Notifiable;

    protected $fillable = [
        'expense',
    ];
}
