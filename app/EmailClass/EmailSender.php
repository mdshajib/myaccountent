<?php

namespace App\EmailClass;

use Mail;
use App\Mail\SendSupport;
use App\Mail\SendInvoice;
use App\Mail\SendNewUser;
use App\Mail\SendNotice;
use App\Mail\SendNotificationt;
use App\Mail\SendTransaction;
use Session;

class EmailSender
{
    public static function SendSupport($sendto,$subject,$message)
    {
        Mail::to($sendto)->send(new SendSupport($subject,$message));
        Session::flash("Mail send successfully.");
        return "Mail send successfully.";
    }

    public static function SendInvoice($sendto,$subject,$message)
    {
        Mail::to($sendto)->send(new SendInvoice($subject,$message));
        Session::flash("Mail send successfully.");
        return "Mail send successfully.";
    }

    public static function SendNewUser($sendto,$subject,$message)
    {
        Mail::to($sendto)->send(new SendNewUser($subject,$message));
        Session::flash("Mail send successfully.");
        return "Mail send successfully.";
    }

    public static function SendNotice($sendto,$subject,$message)
    {
        Mail::to($sendto)->send(new SendNotice($subject,$message));
        Session::flash("Mail send successfully.");
        return "Mail send successfully.";
    }

    public static function SendNotificationt($sendto,$subject,$message)
    {
        Mail::to($sendto)->send(new SendNotificationt($subject,$message));
        Session::flash("Mail send successfully.");
        return "Mail send successfully.";
    }

    public static function SendTransaction($sendto,$subject,$message)
    {
        Mail::to($sendto)->send(new SendTransaction($subject,$message));
        Session::flash("Mail send successfully.");
        return "Mail send successfully.";
    }

}