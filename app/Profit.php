<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Profit extends Model
{
    use Notifiable;

    protected $fillable = [
        'profit',
    ];
}
