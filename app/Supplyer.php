<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplyer extends Model
{
    protected $fillable = [
        'name','email','website','contacts','address',
    ];

    public function brand()
    {
    	return $this->hasOne('App\POS\Brand');
    }
}
