<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Transaction extends Model
{
     use Notifiable;

    protected $fillable = [
        'trxid','income','expense','profit','eachpartner',
    ];
}
