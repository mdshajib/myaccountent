<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Incomesheet extends Model
{
    use Notifiable;

    protected $fillable = [
        'incometype','company','invoice','amount', 'status','paymode',
    ];
}
