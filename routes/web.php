<?php

Route::get('/', function () {
	if(Auth::check())
	{
		return redirect('/home');
	}
    return view('welcome');
});

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home')->middleware('verified');

/*==================================================================
	Income Route
====================================================================*/
Route::get('/commisionbase','Admin\IncomeController@CommisionForm')->name('commision');
Route::post('/commisionbase','Admin\IncomeController@Commision')->name('commision');
Route::get('/consultency','Admin\IncomeController@ConsultencyForm')->name('consultency');
Route::post('/consultency','Admin\IncomeController@Consultency')->name('consultency');

/*============= Update Income Route ================================*/
Route::get('/paidincomes/{id}','Admin\IncomeController@PaidIncomes')->name('paidincomes');

/*==================================================================
	Expenses Route
====================================================================*/

Route::get('/officeaccesories','Admin\ExpenseController@OfficeAccesoriesForm')->name('officeaccesories');
Route::post('/officeaccesories','Admin\ExpenseController@OfficeAccesories')->name('officeaccesories');
Route::get('/officerent','Admin\ExpenseController@OfficeRentForm')->name('officerent');
Route::post('/officerent','Admin\ExpenseController@OfficeRent')->name('officerent');
Route::get('/employeesalary','Admin\EmployeeController@EmployeeSalaryForm')->name('employeesalary');
Route::post('/employeesalary','Admin\EmployeeController@EmployeeSalary')->name('employeesalary');
Route::get('/customergift','Admin\ExpenseController@CustomerGiftForm')->name('customergift');
Route::post('/customergift','Admin\ExpenseController@CustomerGift')->name('customergift');

/*===================== Update Expense Route ==========================*/
Route::get('/paidexpenses/{id}','Admin\ExpenseController@PaidExpenses')->name('paidexpenses');


/*==================================================================
	Setting Route
====================================================================*/
Route::get('/users','Admin\UserController@index')->name('users');
Route::post('/users','Admin\UserController@store')->name('users');
Route::get('/employee','Admin\EmployeeController@index')->name('employee');
Route::post('/employee','Admin\EmployeeController@store')->name('employee');
Route::get('/supplyer','Admin\SupplyerController@index')->name('supplyer');
Route::post('/supplyer','Admin\SupplyerController@store')->name('supplyer');
Route::get('/withdrawal','PartnerProfileController@getWithdrawal')->name('withdrawal');
Route::post('/withdrawal','PartnerProfileController@withdrawal')->name('withdrawal');


Route::get('/closecal','Admin\CloseCalculationController@CloseMonthlyCal')->name('closecal');

/*==================================================================
	Reporting Route
====================================================================*/
Route::get('/incomereportmonthly','Admin\ReportController@IncomeReportMonthly')->name('incomereportmonthly');
Route::get('/expensereportmonthly','Admin\ReportController@ExpenseReportMonthly')->name('expensereportmonthly');
Route::get('/report/{report}','Admin\ReportController@report');


/*==================================================================
	User Profile Route
====================================================================*/
Route::get('/profile','Admin\UserProfileController@index')->name('profile');
Route::post('/changepassword','Admin\UserProfileController@ChangePassword')->name('changepassword');


/*==============================================================
Email Route
================================================================*/
Route::get('/sendemails','Email\EmailController@index')->name('sendemails');
Route::get('/sendemail','Email\EmailController@SendSupportTest')->name('sendemail');

/*==================================================================
		POS Routes
===================================================================*/
Route::get('/category','POS\CategoryController@index')->name('category');
Route::post('/category','POS\CategoryController@store')->name('category');
Route::get('/brand','POS\BrandController@index')->name('brand');
Route::post('/brand','POS\BrandController@store')->name('brand');
Route::get('/model','POS\ModelController@index')->name('model');
Route::post('/model','POS\ModelController@store')->name('model');
Route::get('/product','POS\ProductController@index')->name('product');
Route::post('/product','POS\ProductController@store')->name('product');
Route::get('/purchase','POS\PurchaseController@index')->name('purchase');
Route::post('/purchase','POS\PurchaseController@Purchase')->name('purchase');
Route::get('/sale','POS\SaleController@index')->name('sale');
Route::post('/sale','POS\SaleController@Sale')->name('sale');

Route::get('/duepurchase','POS\PurchaseController@UnpaidPurchase')->name('duepurchase');
Route::get('/duepurchase/{id}','POS\PurchaseController@PaidPurchase');
Route::get('/purchasedetails/{id}','POS\PurchaseController@PurchaseDetails');

Route::get('/duesale','POS\SaleController@UnpaidSale')->name('duesale');
Route::get('/duesale/{id}','POS\SaleController@PaidSale');
Route::get('/saledetails/{id}','POS\SaleController@SaleDetails');

Route::get('/quotation','POS\QuotationController@index')->name('quotation');
Route::post('/quotation','POS\QuotationController@store')->name('quotation');
Route::get('/quotation/{id}','POS\QuotationController@quotationDetails');
Route::get('/allquotation','POS\QuotationController@allQuotation')->name('allquotation');

Route::get('/stock','POS\ProductController@stockList')->name('stock');




/*============== Get Brand, Models and Product By Cat Id using Ajax ================*/
Route::get('/getbrands/{id}','POS\ProductController@getBrands');
Route::get('/getmodels/{id}','POS\ProductController@getModels');
Route::get('/getproducts/{id}','POS\ProductController@getProducts');
Route::get('/getstock/{id}','POS\ProductController@getStock');
Route::get('/getbalance/{userid}','PartnerProfileController@getPartnerBalance');
Route::get('/getprice/{productid}','POS\ProductController@getPrice');




/*==============================================================
Testing Route
================================================================*/

Route::get('/invoice','Admin\InvoiceController@index')->name('invoice');
Route::get('/invoicepdf','Admin\InvoiceController@InvoicePdf')->name('invoicepdf');
Route::post('/invoice','Admin\InvoiceController@Invoice')->name('invoice');

