
		<nav class="navbar header-navbar pcoded-header">
			<div class="navbar-wrapper">
				<div class="navbar-logo">
					<a href="{{route('home')}}">
						<img class="img-fluid" src="{{asset('Backend')}}/assets/images/logo.png" alt="Theme-Logo" />
					</a>
					<a class="mobile-menu" id="mobile-collapse" href="#!">
						<i class="icofont-toggle-off toggleicon"></i>
					</a>
					<a class="mobile-options waves-effect waves-light">
						<i class="icofont-users toggleicon"></i>
					</a>
				</div>
				<div class="navbar-container container-fluid">
					<ul class="nav-left">
						<li class="header-search">
							<div class="main-search morphsearch-search">
								<div class="input-group">
									<span class="input-group-prepend search-close">
										<i class="icofont-close-circled"></i>
									</span>
									<input type="text" class="form-control" placeholder="Enter Keyword">
										<span class="input-group-append search-btn">
										<i class="icofont-search-2"></i>
									</span>
								</div>
							</div>
						</li>
						<li>
							<a href="#!" onclick="javascript:toggleFullScreen()" class="waves-effect waves-light">
								<i class="icofont-expand"></i>
							</a>
						</li>
					</ul>
					<ul class="nav-right">

						<li class="user-profile header-notification">
							<div class="dropdown-primary dropdown">
								<div class="dropdown-toggle" data-toggle="dropdown">
									<img src="{{asset('Backend')}}/assets/images/avatar-1.jpg" class="img-radius" alt="User-Profile-Image">
									<span>
										@if(Auth::check())
										{{ Auth::user()->name }}
										@else
										Session Expired
										@endif
									</span>
									<i class="icofont-circled-down"></i>
								</div>
								<ul class="show-notification profile-notification dropdown-menu" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">

									<li>
										<a href="{{route('profile')}}">
											<i class="icofont-user-suited"></i> Profile
										</a>
									</li>

									<li>
										<a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
											<i class="icofont-exit"></i> Logout
										</a>
										<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
	                                        @csrf
	                                    </form>
									</li>
								</ul>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</nav>

