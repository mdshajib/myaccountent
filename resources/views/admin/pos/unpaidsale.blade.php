
@extends('admin.master')

@section('Content-Title')
unpaid sale
@endsection

@section('Content-Heading')

<i class="icofont-money bg-c-blue"></i>
<div class="d-inline">
	<h5>Unpaid sale</h5>
</div>
@endsection

@section('Main-Content')

<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-block">
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h5>All Unpaid Sale</h5>
							</div>
							<div class="card-block">
								<div class="table-responsive">
									<table class="table table-striped table-hover">
										<thead>
											<tr>
												<th>Invoice</th>
												<th>Grand Total</th>
												<th>Pay Mode</th>
												<th>Date</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											@if($duesales !=null)
												@foreach($duesales as $duesale)
												<tr>
													<td>
														<a class="text-info" href="{{URL::to('saledetails/'.encrypt($duesale->id))}}" title="View Purchase Details">{{ $duesale->invoice }}</a>
														
													</td>
													<td>{{ $duesale->grandtotal }}</td>
													<td>
														@if($duesale->paymode =='banktobank')
														<label class="text-danger"> Bank To Bank</label>

														@elseif($duesale->paymode =='cash')
														<label class="text-success"> {{ $duesale->paymode }}</label>

														@else
														<label class="text-info"> {{ $duesale->paymode }}</label>
														
														@endif
														
													</td>

													<td>{{ $duesale->created_at->format('d-M-Y') }}</td>
													<td>
														<a class="btn btn-out btn-danger btn-mini btn-square" href="{{ URL::to('duesale/'.encrypt($duesale->id))}}">Paid Now</a>
														<a class="btn btn-out waves-effect waves-light hor-grd btn-grd-success btn-mini btn-square" href="{{URL::to('saledetails/'.encrypt($duesale->id))}}" title="View sale Details">View</a>
													</td>
													
												</tr>
												@endforeach
												
											@endif
										</tbody>
									</table>
									{{ $duesales->links() }}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection