@extends('admin.master')

@section('Content-Title')
unpaid purchase
@endsection

@section('Content-Heading')

<i class="icofont-money bg-c-blue"></i>
<div class="d-inline">
	<h5>Unpaid Purchase</h5>
</div>
@endsection

@section('Main-Content')

<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-block">
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h5>All Unpaid Purchase</h5>
							</div>
							<div class="card-block">
								<div class="table-responsive">
									<table class="table table-striped table-hover">
										<thead>
											<tr>
												<th>Invoice</th>
												<th>Grand Total</th>
												<th>Pay Mode</th>
												<th>Date</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											@if($duepurchases !=null)
												@foreach($duepurchases as $duepurchase)
												<tr>
													<td>
														<a class="text-info" href="{{URL::to('purchasedetails/'.encrypt($duepurchase->id))}}" title="View Purchase Details">{{ $duepurchase->invoice }}</a>
														
													</td>
													<td>{{ $duepurchase->grandtotal }}</td>
													<td>
														@if($duepurchase->paymode =='banktobank')
														<label class="text-danger"> Bank To Bank</label>

														@elseif($duepurchase->paymode =='cash')
														<label class="text-success"> {{ $duepurchase->paymode }}</label>

														@else
														<label class="text-info"> {{ $duepurchase->paymode }}</label>
														
														@endif
														
													</td>

													<td>{{ $duepurchase->created_at->format('d-M-Y') }}</td>
													<td>
														<a class="btn btn-out btn-danger btn-mini btn-square" href="{{ URL::to('duepurchase/'.encrypt($duepurchase->id))}}">Paid Now</a>
														<a class="btn btn-out waves-effect waves-light hor-grd btn-grd-success btn-mini btn-square" href="{{URL::to('purchasedetails/'.encrypt($duepurchase->id))}}" title="View Purchase Details">View</a>
													</td>
													
												</tr>
												@endforeach
												
											@endif
										</tbody>
									</table>
									{{ $duepurchases->links() }}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection