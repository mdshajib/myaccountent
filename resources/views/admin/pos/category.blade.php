@extends('admin.master')

@section('Content-Title')
Category
@endsection

@section('Content-Heading')

<i class="icofont-money bg-c-blue"></i>
<div class="d-inline">
	<h5>Category</h5>
</div>
@endsection

@section('Main-Content')

<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-block">
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-block">
								@if($message = Session::get('error'))
									<div class="alert alert-danger alert-block">
										<button type="button" class="close" data-dismiss="alert">×</button>	
									        <strong>{{ $message }}</strong>
									</div>

									@elseif($message = Session::get('success'))
									<div class="alert alert-success alert-block">
										<button type="button" class="close" data-dismiss="alert">×</button>	
									        <strong>{{ $message }}</strong>
									</div>

								@endif

								@if ($errors->any())
								    <div class="alert alert-danger">
								        <ul>
								            @foreach ($errors->all() as $error)
								                <p>{{ $error }}</p>
								            @endforeach
								        </ul>
								    </div>
								@endif
								<form id="category" action="{{route('category')}}" method="post">
									@csrf
									<div class="form-row">
									    <div class="form-group col-md-6">
									      <label for="category">Product Category </label>
									      <input type="text" class="form-control fill" id="category" name="category" value="{{ old('category') }}">
									    </div>
									    <div class="form-group col-md-3">
											<label for=""> </label>
											<button type="submit" class="form-control btn waves-effect waves-light btn-grd-success btn-out btn-skew"><i class="icofont-money"></i> Submit Category</button>
										</div>
	

									</div>
									
								</form>
							</div>
						</div>
					</div>
				</div>

				<hr/>
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h5>All Category</h5>
							</div>
							<div class="card-block">
								<div class="table-responsive">
									<table class="table table-striped table-hover">
										<thead>
											<tr>
												<th>Category</th>
												<th>Date</th>
											</tr>
										</thead>
										<tbody>
											@if($categorys !=null)
												@foreach($categorys as $category)
												<tr>
													<td>{{ $category->category }}</td>
													<td>{{ $category->created_at->format('d-M-Y') }}</td>
													
												</tr>
												@endforeach
											@endif
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection