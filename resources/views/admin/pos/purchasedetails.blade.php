@extends('admin.master')

@section('Content-Title')
purchase details
@endsection

@section('Content-Heading')

<i class="icofont-money bg-c-blue"></i>
<div class="d-inline">
	<h5>Purchase Details</h5>
</div>
@endsection

@section('Main-Content')

<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-block">
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h5>Purchase Details</h5>
							</div>
							<div class="card-block">
								<div class="row">
									<div class="col-md-3">
										<label>Invoice : {{ $purchase->invoice }}</label>
									</div>
									<div class="col-md-3">
										<label>Grand Total : {{ $purchase->grandtotal }}</label>
									</div>
									<div class="col-md-3">
										<label>Pay Mode: 
											@if($purchase->paymode =='banktobank')
												<label class="text-danger"> Bank To Bank</label>

												@elseif($purchase->paymode =='cash')
												<label class="text-success"> {{ $purchase->paymode }}</label>

												@else
												<label class="text-info"> {{ $purchase->paymode }}</label>
											@endif
										</label>
									</div>
									<div class="col-md-3">
										<label>Status : 
											@if($purchase->status =='unpaid')
												<a class="btn btn-out btn-danger btn-mini btn-square" href="{{ URL::to('duepurchase/'.encrypt($purchase->id))}}">Paid Now</a>
											@else
												<label class="text-info">
													{{ $purchase->status }}
												</label>
											@endif
										</label>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h5>Product Details</h5>
							</div>
							<div class="card-block">
								<div class="table-responsive">
									<table class="table table-striped table-hover">
										<thead>
											<tr>
												<th>Product</th>
												<th>Unit</th>
												<th>Unit Price</th>
												<th>Total</th>
												
											</tr>
										</thead>
										<tbody>
											@if($details !=null)
												@foreach($details as $detail)
												<tr>
													<td>{{ $detail->product->product }}</td>
													<td>{{ $detail->unit }}</td>
													<td>{{ $detail->unitprice }}</td>
													<td>{{ $detail->totalprice }}</td>
												</tr>
												@endforeach
											@endif
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection