@extends('admin.master')

@section('Content-Title')
Quotation
@endsection

@section('Content-Heading')

<i class="icofont-money bg-c-blue"></i>
<div class="d-inline">
	<h5>Quotation</h5>
</div>
@endsection

@section('Main-Content')

<?php 
$quot=date('y').rand(10000,9000);
?>

<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-block">
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h5>Quotation</h5>
							</div>
							<div class="card-block">
								@if($message = Session::get('error'))
									<div class="alert alert-danger alert-block">
										<button type="button" class="close" data-dismiss="alert">×</button>	
									        <strong>{{ $message }}</strong>
									</div>

									@elseif($message = Session::get('success'))
									<div class="alert alert-success alert-block">
										<button type="button" class="close" data-dismiss="alert">×</button>	
									        <strong>{{ $message }}</strong>
									</div>

								@endif

								@if ($errors->any())
								    <div class="alert alert-danger">
								        <ul>
								            @foreach ($errors->all() as $error)
								                <p>{{ $error }}</p>
								            @endforeach
								        </ul>
								    </div>
								@endif

									<div class="form-row">
										<div class="form-group col-md-2">
											<label for="quotation">Quotation </label><br/>
											<label class="text-success" id="quotations">{{$quot}}</label>
										</div>
										<div class="form-group col-md-2">
											<label for="grandtotals">Total </label><br/>
											<label class="text-success" id="grandtotals">0</label>
										</div>
										<div class="form-group col-md-2">
											<label for="customers">Customers </label>
									      <select id="category" class="form-control" name="customers">
									      		<option value="">-------Select-------</option>
												@if($customers !=null)
													@foreach($customers as $customer)
														<option value="{{ $customer->id }}" {{(old('customers')== $customer->id)? 'selected':''}}>{{ $customer->customer }}</option>
													@endforeach
												@endif
			                                </select>
										</div>
										<div class="form-group col-md-3">
											<label for="customername">Customer </label>
											<input type="text" class="form-control" id="customername" name="customername" value="{{ old('customername') }}" >
										</div>
										<div class="form-group col-md-3">
											<label for="customeraddress">Address </label>
											<input type="text"  class="form-control" id="customeraddress" name="customeraddress" value="{{ old('customeraddress') }}" >
										</div>
									

									</div>
									<hr/>

									<div class="form-row">
									    <div class="form-group col-md-2">
											<label for="category">Category </label>
									      <select id="category" class="form-control" name="category">
									      		<option value="">-------Select-------</option>
												@if($categorys !=null)
													@foreach($categorys as $category)
														<option value="{{ $category->id }}" {{(old('category')== $category->id)? 'selected':''}}>{{ $category->category }}</option>
													@endforeach
												@endif
			                                </select>
										</div>

									    <div class="form-group col-md-2">
											<label for="brand">Brand </label>
									      <select id="brand" class="form-control" name="brand">
									      		<option value="">-------Select-------</option>
			                                </select>
										</div>

									    <div class="form-group col-md-2">
											<label for="product">Product </label>
									      <select id="product" class="form-control" name="product">
									      		<option value="">-------Select-------</option>
			                                </select>
										</div>
										<div class="form-group col-md-2">
											<label for="unit">Unit </label>
											<input type="number" class="form-control" id="unit" name="unit" value="{{ old('unit') }}" >
										</div>
										<div class="form-group col-md-1">
											<label id="currency"> </label><br/>
											<label id="pprice"> </label>
											
										</div>
										<div class="form-group col-md-1">
											<label for="percent">Percent </label>
											<input type="number" onchange="calpercent();" onkeyup="calpercent();" class="form-control" id="percent" name="percent" value="{{ old('percent') }}" >
										</div>
										<div class="form-group col-md-2">
											<label for="price">Price </label>
											<input type="number" class="form-control" id="price" name="price" value="{{ old('price') }}" >
										</div>

										

									</div>
									<div class="form-row">
										<div class="form-group col-md-12">
											<label for=""> </label>
											<button id="additem" class="form-control btn waves-effect waves-light hor-grd btn-grd-info btn-out">Add Item</button>
										</div>
									</div>

							</div>
						</div>
					</div>
				</div>

				<hr/>
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<button id="finish" class="btn waves-effect waves-light btn-grd-info btn-out-dashed">Finish Quotation</button>
							</div>
							<div class="card-block">
								<div class="table-responsive">
									<form id="quotationform" name="quotationform" method="post" action="{{route('quotation')}}">
										@csrf
										<table class="table table-hover">
											<thead>
												<tr>
													<th>Product</th>
													<th>Brand</th>
													<th>Unit</th>
													<th>Price</th>
													<th>Total Price</th>
												</tr>
											</thead>
											<tbody id="tablecell">

											</tbody>
										</table>
										<input type="hidden" name="quotation" id="quotation">
										<input type="hidden" name="grandtotal" id="grandtotal">
										<input type="hidden" name="customer" id="customer">
										<input type="hidden" name="address" id="address">
									</form>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).on('change','#category',function(){
		var catid= document.getElementById("category").value;
		$('#brand option').remove();
		$.ajax({
			type:'GET',
			url:'/getbrands/'+catid,
			dataType:'json',
			success:function(data){
				$('#brand').append($('<option value="">-------Select-------</option>'));
				$.each(data,function(k,v){
					$('#brand').append($('<option>',{value:k,text:v}));
				});
			},
			error:function(error)
			{
				$('#brand').append($('<option value="">-------Select-------</option>'));
				
			}

		});
	});

	$(document).on('change','#brand',function(){
		var brandid= document.getElementById("brand").value;
		$('#product option').remove();
		$.ajax({
			type:'GET',
			url:'/getproducts/'+brandid,
			dataType:'json',
			success:function(data){
				$('#product').append($('<option value="">-------Select-------</option>'));
				$.each(data,function(k,v){
					$('#product').append($('<option>',{value:k,text:v}));
				});
			},
			error:function(error)
			{
				$('#product').append($('<option value="">-------Select-------</option>'));
				
			}

		});
	});

	$(document).on('change','#product',function(){
		var productid= document.getElementById("product").value;
		$.ajax({
			type:'GET',
			url:'/getprice/'+productid,
			dataType:'json',
			success:function(data){
				document.getElementById("currency").innerText=data['currency'];
				document.getElementById("pprice").innerText=data['price'];
			},
			error:function(error)
			{
				document.getElementById("pprice").innerText=0;
			}

		});
	});

	function calpercent()
	{
		var price=+document.getElementById('pprice').innerText;
		var percent=document.getElementById("percent").value;
		var cal = ((percent/100)*price)+price;
		document.getElementById("price").value=cal;
	}


	$(document).on('click','#additem',function(){
		var brand=document.getElementById("brand").value;
		var brandname=$('#brand option:selected').text();
		var product=document.getElementById("product").value;
		var productname=$('#product option:selected').text();
		var unit=document.getElementById("unit").value;
		var price=document.getElementById("price").value;
		var total=unit*price;
		var gtotal =+document.getElementById('grandtotals').innerText;
		var grandtotal = gtotal+total;
		document.getElementById("grandtotals").innerText=grandtotal;
		if(brand !='' && product !='' && unit !='' && price !='')
		{

			$('#tablecell').append($(
				'<tr>'+
				'<td><input type="text" style="border:none;width:100%;" value="'+productname+'" readonly>'+
				'<input type="hidden" name="product[]" value="'+product+'"'+
				'</td>'+

				'<td><input type="text" style="border:none;width:100%;" value="'+brandname+'" readonly>'+
				'<input type="hidden" name="brand[]" value="'+brand+'">'+
				'</td>'+

				'<td><input type="text" name="unit[]" style="border:none;width:100%;" value="'+unit+'" readonly>'+
				'</td>'+

				'<td><input type="text" name="price[]" style="border:none;width:100%;" value="'+price+'" readonly>'+
				'</td>'+

				'<td><input type="text" name="total[]" style="border:none;width:100%;" value="'+total+'" readonly>'+
				'</td>'+

				'</tr>'
				));
		}
		else
		{
			alert('Each field are required !!!');
		}

	});


$(document).on('click','#finish',function(){
	var quotation=document.getElementById("quotations").innerText;
	var grandtotal=document.getElementById("grandtotals").innerText;
	var customer=document.getElementById("customername").value;
	var address=document.getElementById("customeraddress").value;
	if(quotation !="" && grandtotal !="" && customer !="" && address !="")
	{
		document.getElementById("quotation").value=quotation;
		document.getElementById("grandtotal").value=grandtotal;
		document.getElementById("customer").value=customer;
		document.getElementById("address").value=address;
		document.quotationform.submit();
	}
	else
	{
		alert('All Field are required !!');
	}
});

</script>
@endsection