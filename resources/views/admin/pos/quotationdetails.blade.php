@extends('admin.master')

@section('Content-Title')
Quotation details
@endsection

@section('Content-Heading')

<i class="icofont-money bg-c-blue"></i>
<div class="d-inline">
	<h5>Quotation Details</h5>
</div>
@endsection

@section('Main-Content')

<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-block">
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h5>Quotation Details</h5>
							</div>
							<div class="card-block">
								<div class="row">
									<div class="col-md-2">
										<label>Quotation : {{ $details->quotation }}</label>
									</div>
									<div class="col-md-2">
										<label>Grand Total : {{ $details->grandtotal }}</label>
									</div>
									<div class="col-md-3">
										<label>Customer :  {{ $details->customername }} </label>
									</div>
									<div class="col-md-3">
										<label>Address : {{ $details->address }}</label>
									</div>
									<div class="col-md-2">
										<a class="btn btn-out btn-danger btn-mini btn-square" href="">Place to Order</a>
										<a class="btn btn-out btn-success btn-mini btn-square" href="">PDF</a>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h5>Quotation Details</h5>
							</div>
							<div class="card-block">
								<div class="table-responsive">
									<table class="table table-striped table-hover">
										<thead>
											<tr>
												<th>Product</th>
												<th>Unit</th>
												<th>Unit Price</th>
												<th>Total</th>
												
											</tr>
										</thead>
										<tbody>
											@if($details->quotDetails !=null)
												@foreach($details->quotDetails as $detail)
												<tr>
													<td>{{ $detail->product->product }}</td>
													<td>{{ $detail->unit }}</td>
													<td>{{ $detail->unitprice }}</td>
													<td>{{ $detail->totalprice }}</td>
												</tr>
												@endforeach
											@endif
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection