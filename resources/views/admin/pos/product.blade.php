@extends('admin.master')

@section('Content-Title')
Product
@endsection

@section('Content-Heading')

<i class="icofont-money bg-c-blue"></i>
<div class="d-inline">
	<h5>Products</h5>
</div>
@endsection

@section('Main-Content')

<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-block">
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h5>Add New Product</h5>
							</div>
							<div class="card-block">
								@if($message = Session::get('error'))
									<div class="alert alert-danger alert-block">
										<button type="button" class="close" data-dismiss="alert">×</button>	
									        <strong>{{ $message }}</strong>
									</div>

									@elseif($message = Session::get('success'))
									<div class="alert alert-success alert-block">
										<button type="button" class="close" data-dismiss="alert">×</button>	
									        <strong>{{ $message }}</strong>
									</div>

								@endif

								@if ($errors->any())
								    <div class="alert alert-danger">
								        <ul>
								            @foreach ($errors->all() as $error)
								                <p>{{ $error }}</p>
								            @endforeach
								        </ul>
								    </div>
								@endif
								<form id="product" action="{{route('product')}}" method="post">
									@csrf
									<div class="form-row">
									    <div class="form-group col-md-3">
											<label for="category">Category </label>
									      <select id="category" class="form-control" name="category">
									      		<option value="">-------Select-------</option>
												@if($categories !=null)
													@foreach($categories as $category)
														<option value="{{ $category->id }}" {{(old('category')== $category->id)? 'selected':''}}>{{ $category->category }}</option>
													@endforeach
												@endif
			                                </select>
										</div>

									    <div class="form-group col-md-3">
											<label for="brand">Brand </label>
									      <select id="brand" class="form-control" name="brand">
									      		<option value="">-------Select-------</option>
			                                </select>
										</div>
										<div class="form-group col-md-3">
											<label for="models">Model </label>
									      <select id="models" class="form-control" name="models">
									      		<option value="">-------Select-------</option>
			                                </select>
										</div>


									    <div class="form-group col-md-3">
									      <label for="product">Product</label>
									      <input type="text" class="form-control" id="product" name="product" value="{{ old('product') }}">
									    </div>

									</div>
									<div class="form-row">
										

										<div class="form-group col-md-3">
											<label for="description">Description </label>
											<input type="text" class="form-control" id="description" name="description" value="{{ old('description') }}" >
										</div>
										<div class="form-group col-md-2">
											<label for="origin">Origin </label>
											<input type="text" class="form-control" id="origin" name="origin" value="{{ old('origin') }}" >
										</div>
										<div class="form-group col-md-2">
											<label for="currency">Currency </label>
									      <select id="currency" class="form-control" name="currency">
									      		<option value="">-------Select-------</option>
									      		<option value="USD">USD</option>
									      		<option value="EURO">EURO</option>
									      		<option value="TK">TK</option>
			                                </select>
										</div>
										<div class="form-group col-md-2">
											<label for="price">Price </label>
											<input type="text" class="form-control" id="price" name="price" value="{{ old('price') }}" >
										</div>



										<div class="form-group col-md-3">
											<label for=""> </label>
											<button type="submit" class="form-control btn waves-effect waves-light hor-grd btn-grd-info btn-out btn-skew"><i class="icofont-money"></i> Add New Product</button>
										</div>


									</div>
								</form>
							</div>
						</div>
					</div>
				</div>

				<hr/>
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h5>All Products</h5>
							</div>
							<div class="card-block">
								<div class="table-responsive">
									<table class="table table-striped table-hover">
										<thead>
											<tr>
												<th>Product</th>
												<th>Brand</th>
												<th>Model</th>
												<th>Description</th>
												<th>Origin</th>
												<th>Currency</th>
												<th>Price</th>
											</tr>
										</thead>
										<tbody>
											@if($products !=null)
												@foreach($products as $product)
												<tr>
													<td>{{ $product->product }}</td>
													<td>{{ $product->brand->brand }}</td>
													<td>{{ $product->models->model }}</td>
													<td>{{ $product->description}}</td>
													<td>{{ $product->origin}}</td>
													<td>{{ $product->currency}}</td>
													<td>{{ $product->price}}</td>
												</tr>
												@endforeach
											@endif
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).on('change','#category',function(){
		var catid= document.getElementById("category").value;
		$('#brand option').remove();
		$.ajax({
			type:'GET',
			url:'/getbrands/'+catid,
			dataType:'json',
			success:function(data){
				$('#brand').append($('<option>-------Select-------</option>'));
				$.each(data,function(k,v){
					$('#brand').append($('<option>',{value:k,text:v}));
				});
			},
			error:function(error)
			{
				$('#brand').append($('<option>-------Select-------</option>'));
				
			}

		});
	});

	$(document).on('change','#brand',function(){
		var brandid= document.getElementById("brand").value;
		$('#models option').remove();
		$.ajax({
			type:'GET',
			url:'/getmodels/'+brandid,
			dataType:'json',
			success:function(data){
				$('#models').append($('<option>-------Select-------</option>'));
				$.each(data,function(k,v){
					$('#models').append($('<option>',{value:k,text:v}));
				});
			},
			error:function(error)
			{
				$('#models').append($('<option>-------Select-------</option>'));
				
			}

		});
	});
</script>
@endsection