@extends('admin.master')

@section('Content-Title')
Stock list
@endsection

@section('Content-Heading')

<i class="icofont-money bg-c-blue"></i>
<div class="d-inline">
	<h5>Stock List</h5>
</div>
@endsection

@section('Main-Content')

<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-block">
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h5>Stock List</h5>
							</div>
							<div class="card-block">
								<div class="table-responsive">
									<table class="table table-striped table-hover">
										<thead>
											<tr>
												<th>Category</th>
												<th>Brand</th>
												<th>Model</th>
												<th>Product</th>
												<th>Quantity</th>
											</tr>
										</thead>
										<tbody>
                                            @if($stocks !=null)
												@foreach($stocks as $stock)
												<tr>
													<td>{{ $stock->product->brand->category->category }}</td>
													<td>{{ $stock->product->brand->brand }}</td>
													<td>{{ $stock->product->models->model }}</td>
													<td>{{ $stock->product->product }}</td>
													<td>
														@if($stock->stock ==0)
														<label class="text-danger"> Out of stock</label>
														@elseif($stock->stock <=5)
														<label class="text-warning"> {{ $stock->stock }} Available</label>
														@else
														<label class="text-success"> {{ $stock->stock }} Available</label>
														@endif
													</td>
													
												</tr>
												@endforeach
											@endif
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection