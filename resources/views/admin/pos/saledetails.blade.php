@extends('admin.master')

@section('Content-Title')
Sale details
@endsection

@section('Content-Heading')

<i class="icofont-money bg-c-blue"></i>
<div class="d-inline">
	<h5>Sale Details</h5>
</div>
@endsection

@section('Main-Content')

<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-block">
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h5>Sale Details</h5>
							</div>
							<div class="card-block">
								<div class="row">
									<div class="col-md-3">
										<label>Invoice : {{ $sale->invoice }}</label>
									</div>
									<div class="col-md-3">
										<label>Grand Total : {{ $sale->grandtotal }}</label>
									</div>
									<div class="col-md-3">
										<label>Pay Mode: 
											@if($sale->paymode =='banktobank')
												<label class="text-danger"> Bank To Bank</label>

												@elseif($sale->paymode =='cash')
												<label class="text-success"> {{ $sale->paymode }}</label>

												@else
												<label class="text-info"> {{ $sale->paymode }}</label>
											@endif
										</label>
									</div>
									<div class="col-md-3">
										<label>Status : 
											@if($sale->status =='unpaid')
												<a class="btn btn-out btn-danger btn-mini btn-square" href="{{ URL::to('duesale/'.encrypt($sale->id))}}">Paid Now</a>
											@else
												<label class="text-info">
													{{ $sale->status }}
												</label>
											@endif
										</label>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h5>Product Details</h5>
							</div>
							<div class="card-block">
								<div class="table-responsive">
									<table class="table table-striped table-hover">
										<thead>
											<tr>
												<th>Product</th>
												<th>Unit</th>
												<th>Unit Price</th>
												<th>Total</th>
												
											</tr>
										</thead>
										<tbody>
											@if($details !=null)
												@foreach($details as $detail)
												<tr>
													<td>{{ $detail->product->product }}</td>
													<td>{{ $detail->unit }}</td>
													<td>{{ $detail->unitprice }}</td>
													<td>{{ $detail->totalprice }}</td>
												</tr>
												@endforeach
											@endif
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection