@extends('admin.master')

@section('Content-Title')
Sale
@endsection

@section('Content-Heading')

<i class="icofont-money bg-c-blue"></i>
<div class="d-inline">
	<h5>Sale</h5>
</div>
@endsection

@section('Main-Content')

<?php 
$inv=time();
?>

<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-block">
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h5>Sales</h5>
							</div>
							<div class="card-block">
								@if($message = Session::get('error'))
									<div class="alert alert-danger alert-block">
										<button type="button" class="close" data-dismiss="alert">×</button>	
									        <strong>{{ $message }}</strong>
									</div>

									@elseif($message = Session::get('success'))
									<div class="alert alert-success alert-block">
										<button type="button" class="close" data-dismiss="alert">×</button>	
									        <strong>{{ $message }}</strong>
									</div>

								@endif

								@if ($errors->any())
								    <div class="alert alert-danger">
								        <ul>
								            @foreach ($errors->all() as $error)
								                <p>{{ $error }}</p>
								            @endforeach
								        </ul>
								    </div>
								@endif

									<div class="form-row">
										<div class="form-group col-md-1">
											<label for="invoices">Invoice </label><br/>
											<label class="text-success" id="invoices">{{$inv}}</label>
										</div>
										<div class="form-group col-md-1">
											<label for="grandtotals">Total </label><br/>
											<label class="text-success" id="grandtotals">0</label>
										</div>
										<div class="form-group col-md-3">
											<label for="customername">Customer </label>
											<input type="text" class="form-control" id="customername" name="customername" value="{{ old('customername') }}" >
										</div>
										<div class="form-group col-md-3">
											<label for="customeraddress">Address </label>
											<input type="text"  class="form-control" id="customeraddress" name="customeraddress" value="{{ old('customeraddress') }}" >
										</div>
										
										<div class="form-group col-md-2">
											 <label for="statuss">Payment Status</label>
											<select id="statuss" class="form-control" name="statuss">
			                                    <option value="">-------Select-------</option>
			                                    <option value="paid" {{ old('statuss') == 'paid' ? 'selected' : '' }}>Paid</option>
			                                    <option value="unpaid" {{ old('statuss') == 'unpaid' ? 'selected' : '' }}>Unpaid</option>
			                                    
			                                </select>
										</div>

										<div class="form-group col-md-2">
									      <label for="paymodes">Payment Mode</label>
											<select id="paymodes" class="form-control" name="paymodes">
												<option value="">-------Select-------</option>
			                                    <option value="cash" {{ old('paymodes') == 'cash' ? 'selected' : '' }}>Cash</option>
			                                    <option value="chaque" {{ old('paymodes') == 'chaque' ? 'selected' : '' }}>Chaque</option>
			                                    <option value="banktobank" {{ old('paymodes') == 'banktobank' ? 'selected' : '' }}>Bank To Bank</option>
			                                </select>
									    </div>

									</div>
									<hr/>

									<div class="form-row">
									    <div class="form-group col-md-2">
											<label for="category">Category </label>
									      <select id="category" class="form-control" name="category">
									      		<option value="">-------Select-------</option>
												@if($categories !=null)
													@foreach($categories as $category)
														<option value="{{ $category->id }}" {{(old('category')== $category->id)? 'selected':''}}>{{ $category->category }}</option>
													@endforeach
												@endif
			                                </select>
										</div>

									    <div class="form-group col-md-2">
											<label for="brand">Brand </label>
									      <select id="brand" class="form-control" name="brand">
									      		<option value="">-------Select-------</option>
			                                </select>
										</div>

									    <div class="form-group col-md-2">
											<label for="product">Product </label>
									      <select id="product" class="form-control" name="product">
									      		<option value="">-------Select-------</option>
			                                </select>
										</div>
										<div class="form-group col-md-1">
											<label for="unit">Stock </label><br/>
											<label class="text-success text-center" id="available">0</label>
										</div>
										<div class="form-group col-md-1">
											<label for="unit">Unit </label>
											<input type="number" class="form-control" onkeyup="QuantityCheck()" id="unit" name="unit" value="{{ old('unit') }}" >
										</div>
										<div class="form-group col-md-2">
											<label for="price">Price </label>
											<input type="number" class="form-control" id="price" name="price" value="{{ old('price') }}" >
										</div>

										<div class="form-group col-md-2">
											<label for=""> </label>
											<button id="additem" class="form-control btn waves-effect waves-light hor-grd btn-grd-info btn-out btn-skew">Add Item</button>
										</div>

									</div>

							</div>
						</div>
					</div>
				</div>

				<hr/>
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<button id="finish" class="btn waves-effect waves-light btn-grd-info btn-out-dashed">Finish Sale</button>
							</div>
							<div class="card-block">
								<div class="table-responsive">
									<form id="sale" name="sale" method="post" action="{{route('sale')}}">
										@csrf
										<table class="table table-striped table-hover">
											<thead>
												<tr>
													<th>Product</th>
													<th>Brand</th>
													<th>Unit</th>
													<th>Price</th>
													<th>Total Price</th>
												</tr>
											</thead>
											<tbody id="tablecell">

											</tbody>
										</table>
										<input type="hidden" name="invoice" id="invoice">
										<input type="hidden" name="grandtotal" id="grandtotal">
										<input type="hidden" name="customer" id="customer">
										<input type="hidden" name="address" id="address">
										<input type="hidden" name="paymode" id="paymode">
										<input type="hidden" name="status" id="status">
									</form>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).on('change','#category',function(){
		var catid= document.getElementById("category").value;
		$('#brand option').remove();
		$.ajax({
			type:'GET',
			url:'/getbrands/'+catid,
			dataType:'json',
			success:function(data){
				$('#brand').append($('<option value="">-------Select-------</option>'));
				$.each(data,function(k,v){
					$('#brand').append($('<option>',{value:k,text:v}));
				});
			},
			error:function(error)
			{
				$('#brand').append($('<option value="">-------Select-------</option>'));
				
			}

		});
	});

	$(document).on('change','#brand',function(){
		var brandid= document.getElementById("brand").value;
		$('#product option').remove();
		$.ajax({
			type:'GET',
			url:'/getproducts/'+brandid,
			dataType:'json',
			success:function(data){
				$('#product').append($('<option value="">-------Select-------</option>'));
				$.each(data,function(k,v){
					$('#product').append($('<option>',{value:k,text:v}));
				});
			},
			error:function(error)
			{
				$('#product').append($('<option value="">-------Select-------</option>'));
				
			}

		});
	});


	$(document).on('change','#product',function(){
		var productid= document.getElementById("product").value;
		$.ajax({
			type:'GET',
			url:'/getstock/'+productid,
			dataType:'json',
			success:function(data){
				document.getElementById('available').innerText=data;
			},
			error:function(error)
			{
				document.getElementById('available').innerText=0;
				
			}

		});
	});


	$(document).on('click','#additem',function(){
		var brand=document.getElementById("brand").value;
		var brandname=$('#brand option:selected').text();
		var product=document.getElementById("product").value;
		var productname=$('#product option:selected').text();
		var unit=document.getElementById("unit").value;
		var price=document.getElementById("price").value;
		var total=unit*price;
		var gtotal =+document.getElementById('grandtotals').innerText;
		var grandtotal = gtotal+total;
		document.getElementById("grandtotals").innerText=grandtotal;

		if(brand !='' && product !='' && unit !='' && price !='')
		{

			$('#tablecell').append($(
				'<tr>'+
				'<td><input type="text" style="border:none;width:100%;" value="'+productname+'" readonly>'+
				'<input type="hidden" name="product[]" value="'+product+'"'+
				'</td>'+

				'<td><input type="text" style="border:none;width:100%;" value="'+brandname+'" readonly>'+
				'<input type="hidden" name="brand[]" value="'+brand+'">'+
				'</td>'+

				'<td><input type="text" name="unit[]" style="border:none;width:100%;" value="'+unit+'" readonly>'+
				'</td>'+

				'<td><input type="text" name="price[]" style="border:none;width:100%;" value="'+price+'" readonly>'+
				'</td>'+

				'<td><input type="text" name="total[]" style="border:none;width:100%;" value="'+total+'" readonly>'+
				'</td>'+

				'</tr>'
				));
		}
		else
		{
			alert('Each field are required !!!');
		}

	});


$(document).on('click','#finish',function(){
	var invoice=document.getElementById("invoices").innerText;
	var grandtotal=document.getElementById("grandtotals").innerText;
	var customer=document.getElementById("customername").value;
	var address=document.getElementById("customeraddress").value;
	var paymode=document.getElementById("paymodes").value;
	var status=document.getElementById("statuss").value;
	if(invoice !="" && grandtotal !="" && paymode !="" && status !="")
	{
		document.getElementById("invoice").value=invoice;
		document.getElementById("grandtotal").value=grandtotal;
		document.getElementById("customer").value=customer;
		document.getElementById("address").value=address;
		document.getElementById("paymode").value=paymode;
		document.getElementById("status").value=status;
		document.sale.submit();
	}
	else
	{
		alert('All Field are required !!');
	}
});

function QuantityCheck()
	{
		var maxquantity=+document.getElementById('available').innerText;
		var quantity=+document.getElementById('unit').value;
		if(quantity>maxquantity)
		{
			alert("Not enought stock ! stock available : "+maxquantity);
			document.getElementById('unit').value='';
		}
		else if(maxquantity<=0)
			document.getElementById('unit').value=''; 
		//alert(maxquantity+" - "+quantity);
	}

</script>
@endsection