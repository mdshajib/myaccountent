@extends('admin.master')

@section('Content-Title')
Model
@endsection

@section('Content-Heading')

<i class="icofont-money bg-c-blue"></i>
<div class="d-inline">
	<h5>Model</h5>
</div>
@endsection

@section('Main-Content')

<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-block">
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-block">
								@if($message = Session::get('error'))
									<div class="alert alert-danger alert-block">
										<button type="button" class="close" data-dismiss="alert">×</button>	
									        <strong>{{ $message }}</strong>
									</div>

									@elseif($message = Session::get('success'))
									<div class="alert alert-success alert-block">
										<button type="button" class="close" data-dismiss="alert">×</button>	
									        <strong>{{ $message }}</strong>
									</div>

								@endif

								@if ($errors->any())
								    <div class="alert alert-danger">
								        <ul>
								            @foreach ($errors->all() as $error)
								                <p>{{ $error }}</p>
								            @endforeach
								        </ul>
								    </div>
								@endif
								<form id="model-form" action="{{route('model')}}" method="post">
									@csrf
									<div class="form-row">

										<div class="form-group col-md-3">
											<label for="category">Category </label>
									      	<select id="category" class="form-control" name="category">
									      		<option value="">-------Select-------</option>
												@if($categorys !=null)
													@foreach($categorys as $category)
														<option value="{{ $category->id }}" {{(old('category')== $category->id)? 'selected':''}}>{{ $category->category }}</option>
													@endforeach
												@endif
			                                </select>
										</div>
									    <div class="form-group col-md-3">
											<label for="brand">Brand </label>
									      <select id="brand" class="form-control" name="brand">
									      		<option value="">-------Select-------</option>
												
			                                </select>
										</div>

										<div class="form-group col-md-3">
									      <label for="model">Model No </label>
									      <input type="text" class="form-control fill" id="model" name="model" value="{{ old('model') }}">
									    </div>

									    <div class="form-group col-md-3">
											<label for=""> </label>
											<button type="submit" class="form-control btn waves-effect waves-light btn-grd-success btn-out btn-skew"><i class="icofont-money"></i> Submit Model</button>
										</div>
	

									</div>
									
								</form>
							</div>
						</div>
					</div>
				</div>

				<hr/>
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h5>All Brands</h5>
							</div>
							<div class="card-block">
								<div class="table-responsive">
									<table class="table table-striped table-hover">
										<thead>
											<tr>
												<th>Category</th>
												<th>Brand</th>
												<th>Model</th>
												<th>Date</th>
											</tr>
										</thead>
										<tbody>
											@if($models !=null)
												@foreach($models as $model)
												<tr>
													<td>{{ $model->brand->category->category }}</td>
													<td>{{ $model->brand->brand }}</td>
													<td>{{ $model->model }}</td>
													<td>{{ $model->created_at->format('d-M-Y') }}</td>
													
												</tr>
												@endforeach
											@endif
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).on('change','#category',function(){
		var catid= document.getElementById("category").value;
		$('#brand option').remove();
		$.ajax({
			type:'GET',
			url:'/getbrands/'+catid,
			dataType:'json',
			success:function(data){
				$.each(data,function(k,v){
					$('#brand').append($('<option>',{value:k,text:v}));
				});
			},
			error:function(error)
			{
				$('#brand').append($('<option>-------Select-------</option>'));
				
			}

		});
	});
</script>
@endsection