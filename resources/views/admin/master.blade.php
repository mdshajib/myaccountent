<!DOCTYPE html>
<html lang="en">
<head>
<title>@yield('Content-Title') | 4SAT</title>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />

<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Quicksand:500,700" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="{{asset('Backend')}}/bower_components/bootstrap/css/bootstrap.min.css">


<link rel="stylesheet" href="{{asset('Backend')}}/assets/pages/waves/css/waves.min.css" type="text/css" media="all">
<link rel="stylesheet" href="{{asset('Backend')}}/assets/css/custome.css" type="text/css">

<link rel="stylesheet" type="text/css" href="{{asset('Backend')}}/assets/icon/icofont/icofont.css">


<link rel="stylesheet" type="text/css" href="{{asset('Backend')}}/assets/css/datepicker3.css">

<link rel="stylesheet" type="text/css" href="{{asset('Backend')}}/assets/css/style.css">
<link rel="stylesheet" type="text/css" href="{{asset('Backend')}}/assets/css/widget.css">
<link rel="stylesheet" type="text/css" href="{{asset('Backend')}}/assets/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="{{asset('Backend')}}/assets/css/pages.css">
<link rel="stylesheet" type="text/css" href="{{asset('Backend')}}/assets/css/sweetalert2.min.css">
</head>
<body>

<div class="loader-bg">
	<div class="loader-bar"></div>
</div>

<div id="pcoded" class="pcoded">
	<div class="pcoded-overlay-box"></div>
	<div class="pcoded-container navbar-wrapper">
		@include('admin.inc.header')


		<div class="pcoded-main-container">
			<div class="pcoded-wrapper">
				@include('admin.inc.sidebar')

				<div class="pcoded-content">

					<div class="page-header card">
						<div class="row align-items-end">
							<div class="col-lg-8">
								<div class="page-header-title">
									@yield('Content-Heading')
								</div>
							</div>
						</div>
					</div>

					<div class="pcoded-inner-content">
						<div class="main-body">
							<div class="page-wrapper">
								<div class="page-body">
									@yield('Main-Content')
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<script type="text/javascript" src="{{asset('Backend')}}/bower_components/jquery/js/jquery.min.js"></script>
<script type="text/javascript" src="{{asset('Backend')}}/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="{{asset('Backend')}}/bower_components/popper.js/js/popper.min.js"></script>
<script type="text/javascript" src="{{asset('Backend')}}/bower_components/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="{{asset('Backend')}}/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
<script src="{{asset('Backend')}}/assets/js/pcoded.min.js"></script>
<script src="{{asset('Backend')}}/assets/js/vertical/vertical-layout.min.js"></script>
<script type="text/javascript" src="{{asset('Backend')}}/assets/pages/waves/js/waves.min.js"></script>
<script type="text/javascript" src="{{asset('Backend')}}/assets/js/script.min.js"></script>
<script type="text/javascript" src="{{asset('Backend')}}/assets/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="{{asset('Backend')}}/assets/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{{asset('Backend')}}/assets/js/sweetalert2.min.js"></script>


</body>
</html>
