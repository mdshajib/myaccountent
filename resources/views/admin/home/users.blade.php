@extends('admin.master')

@section('Content-Title')
Users
@endsection

@section('Content-Heading')

<i class="icofont-money bg-c-blue"></i>
<div class="d-inline">
	<h5>Users</h5>
</div>
@endsection

@section('Main-Content')

<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-block">
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h5>Add New User</h5>
							</div>
							<div class="card-block">
								@if($message = Session::get('error'))
									<div class="alert alert-danger alert-block">
										<button type="button" class="close" data-dismiss="alert">×</button>	
									        <strong>{{ $message }}</strong>
									</div>

									@elseif($message = Session::get('success'))
									<div class="alert alert-success alert-block">
										<button type="button" class="close" data-dismiss="alert">×</button>	
									        <strong>{{ $message }}</strong>
									</div>

								@endif

								@if ($errors->any())
								    <div class="alert alert-danger">
								        <ul>
								            @foreach ($errors->all() as $error)
								                <p>{{ $error }}</p>
								            @endforeach
								        </ul>
								    </div>
								@endif
								<form id="user" action="{{route('users')}}" method="post">
									@csrf
									<div class="form-row">
									    <div class="form-group col-md-4">
									      <label for="fullname">Full name </label>
									      <input type="text" class="form-control" id="fullname" name="fullname" value="{{ old('fullname') }}">
									    </div>

									    <div class="form-group col-md-4">
									      <label for="email">Email</label>
									      <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}">
									    </div>

									    <div class="form-group col-md-4">
									      <label for="phone">Phone</label>
									      <input type="text" class="form-control" id="phone" name="phone" value="{{ old('phone') }}">
									    </div>


									</div>
									<div class="form-row">
										

										<div class="form-group col-md-4">
											<label for="password">Password </label>
											<input type="text" class="form-control" id="password" name="password" value="{{ old('password') }}" autocomplete="off">
										</div>

										<div class="form-group col-md-4">
									      <label for="role">Role</label>
											<select id="role" class="form-control" name="role">
												<option value="">-------Select-------</option>
			                                    <option value="admin" {{ old('role') == 'admin' ? 'selected' : '' }}>Admin</option>
			                                    <option value="partner" {{ old('role') == 'partner' ? 'selected' : '' }}>Partner</option>
			                                    <option value="user" {{ old('role') == 'user' ? 'selected' : '' }}>User</option>
			                                </select>
									    </div>

										<div class="form-group col-md-4">
											<label for=""> </label>
											<button type="submit" class="form-control btn waves-effect waves-light hor-grd btn-grd-info btn-out btn-skew"><i class="icofont-money"></i> Add New User</button>
										</div>


									</div>
								</form>
							</div>
						</div>
					</div>
				</div>

				<hr/>
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h5>All Users</h5>
							</div>
							<div class="card-block">
								<div class="table-responsive">
									<table class="table table-striped table-hover">
										<thead>
											<tr>
												<th>Name</th>
												<th>Email</th>
												<th>Phone</th>
												<th>Role</th>
												<th>Status</th>
												<th>Date</th>
											</tr>
										</thead>
										<tbody>
											@if($users !=null)
												@foreach($users as $user)
												<tr>
													<td>{{ $user->name }}</td>
													<td>{{ $user->email }}</td>
													<td>{{ $user->phone}}</td>
													<td>
														@if($user->role =='partner')
															<label class="label bg-danger">
																{{ $user->role }}
															</label>
														@else
															<label class="label bg-info">
																{{ $user->role }}
															</label>
														@endif
													</td>
													
													<td>
														@if(!$user->email_verified_at)
															<label class="label bg-warning">
																Not verifyed
															</label>
														@else
															<label class="label bg-success">
																Verifyed
															</label>
														@endif
													</td>
													<td>
														{{ $user->created_at->format('d-M-Y')}}
													</td>
													
												</tr>
												@endforeach
											@endif
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection