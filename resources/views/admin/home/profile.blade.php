@extends('admin.master')

@section('Content-Title')
All Users
@endsection

@section('Content-Heading')
<i class="icofont-users-alt-4 bg-c-blue"></i>
<div class="d-inline">
	<h5>Users Profile</h5>
</div>

@endsection

@section('Main-Content')
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-block">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group row">
							<label class="col-md-2">Full Name</label>
							<div class="col-md-10">
								<label>{{ $profile->name }}</label>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-md-2">Email</label>
							<div class="col-md-10">
								<label>{{ $profile->email }}</label>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-md-2">Phone</label>
							<div class="col-md-10">
								<label>{{ $profile->phone }}</label>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-md-2">Role</label>
							<div class="col-md-10">
								<label>{{ $profile->role }}</label>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



@endsection