<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style type="text/css">
      body{
        height: auto;
        width: 700px;
      }
      .header-area{
        height: 75px;
        width: 700px;
      }
      .logo{
        height: 70px;
        width: 80px;
        float: left;
      }
      .header-text{
        height: 70px;
        width: 620px;
        float: left;
        text-align: center;
      }
      .company-name{
        font-weight: bold;
        font-size: 35px;
        text-decoration: underline;
      }
      .description{
        font-size: 16px;
        font-weight: bold;
      }
      .address-box{
        height: 90px;
        width: 700px;
      }
      .address1{
        height: 90px;
        width: 300px;
        float: left;
        text-align: left;
      }
      
      .address2{
        height: 90px;
        width: 250px;
        float: right;
        text-align: right;
      }
      .address1>p, .address2>p{
        line-height: 5px;
      }

      .type{
        width: 700px;
        height: 40px;
		float: left;
		margin-bottom: 50px;
		overflow: hidden;
		text-align: center;
		margin-top: 10px;
      }

      .data-table{
        width: 700px;
        height: auto;
		overflow: hidden;
		position: absolute;
		margin-top: 30px;
      }
      table{
        width: 100%;
        border-collapse: collapse;
        border:1px solid #000;

      }

      table>thead>tr>th{
        border: 1px solid #000;
      }
      table>tbody>tr>td{
        border-left: 1px solid #000;
        border-right: 1px solid #000;
      }
      th,td{
        white-space: nowrap;
        padding: 8px;
      }
      table tr{
        min-height: 40px;
      }
      tr th,tr td{
        text-align: center;
      }
      tr.tdborder td{
        border:1px solid #000;
      }
      .amount-inword{
        width: 700px;
        height: 30px;
      }
      .signature-area{
        width: 700px;
        height: 70px;
        padding-top: 30px;
      }
      .receiver-sig{
        width: 200px;
        height: 40px;
        float: left;
      }
      .sender-sig{
        width: 350px;
        height: 40px;
        float: right;
      }




    </style>
</head>
<body>
   <div class="header-area">
      <div class="logo">
        <img src="" alt="Logo">
      </div>

      <div class="header-text">
        <div class="company-name">
          4S ADVANCE TECHNOLOGIES LTD.
        </div>
        <div class="description">
          All kinds of Imported Shoe Materials and Shoe Machine’s Spare Parts Supplier.
        </div>
      </div>
    </div>

    <div class="address-box">
      <div class="address1">
        <p>Registered office Address:</p>
        <p>Ka-80, Mohakhali south zone</p>
        <p>Dhaka -1212, Bangladesh.</p>
      </div>

      <div class="address2">
        <p>Present office Address:</p>
        <p>House # 12/D, Bashbari Road</p>
        <p>Mohammadpur, Dhaka-1207</p>
        <p>E-mail:4stradinghouse@gmail.com</p>
      </div>
    </div>

    <div class="type">
      <span style="color: #000;font-size: 18px;">
        Income Report : {{$incomes[0]->created_at->format('M-Y')}}
      </span>
    </div>

    <div class="data-table">
      <table>
        <thead>
          <tr>
            <th>Date</th>
            <th>Income Type</th>
            <th>Company</th>
            <th>Invoice</th>
            <th>Amount</th>
            <th>Pay Mode</th>
          </tr>
        </thead>
        <tbody>
				@foreach($incomes as $income)
				<tr>
					<td>{{ $income->created_at->format('d-m-Y') }}</td>
					<td>{{ $income->incometype }}</td>
					<td>{{ $income->company }}</td>
					<td>{{ $income->invoice }}</td>
					<td>{{ $income->amount }}</td>
					<td>{{ $income->paymode }}</td>
				</tr>
			@endforeach
        </tbody>

      </table>
    </div> 



</body>
</html>