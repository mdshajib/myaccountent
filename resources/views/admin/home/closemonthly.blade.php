@extends('admin.master')

@section('Content-Title')
Monthly Closed
@endsection

@section('Content-Heading')

<i class="icofont-ui-home bg-c-blue"></i>
<div class="d-inline">
	<h5>Monthly Close</h5>
</div>
@endsection

@section('Main-Content')

<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-block">
				<div class="row">

					<div class="col-md-5">
						<div class="p-0 z-depth-bottom-0">
							
							<div class="alert alert-danger alert-block">
								<button type="button" class="close" data-dismiss="alert">×</button>	
							        <strong>{{ $success}}</strong>
							</div>

							
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>



@endsection