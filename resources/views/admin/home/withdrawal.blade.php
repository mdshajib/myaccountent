@extends('admin.master')

@section('Content-Title')
Withdrawal
@endsection

@section('Content-Heading')

<i class="icofont-money bg-c-blue"></i>
<div class="d-inline">
	<h5>Withdrawal</h5>
</div>
@endsection

@section('Main-Content')

<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-block">
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-block">
								@if($message = Session::get('error'))
									<div class="alert alert-danger alert-block">
										<button type="button" class="close" data-dismiss="alert">×</button>	
									        <strong>{{ $message }}</strong>
									</div>

									@elseif($message = Session::get('success'))
									<div class="alert alert-success alert-block">
										<button type="button" class="close" data-dismiss="alert">×</button>	
									        <strong>{{ $message }}</strong>
									</div>

								@endif

								@if ($errors->any())
								    <div class="alert alert-danger">
								        <ul>
								            @foreach ($errors->all() as $error)
								                <p>{{ $error }}</p>
								            @endforeach
								        </ul>
								    </div>
								@endif
								<form id="second" method="post" action="{{route('withdrawal')}}">
									@csrf
									<div class="form-row">
										<div class="form-group col-md-3">
									      <label for="partner">Partner</label>
											<select id="partner" class="form-control" name="partner">
												<option value="">-------Select-------</option>
												@if($partners !=null)
													@foreach($partners as $partner)
														<option value="{{ $partner->id }}" {{(old('partner')== $partner->id)? 'selected':''}}>{{ $partner->name }}</option>
													@endforeach
												@endif
			                                </select>
									    </div>
									    <div class="form-group col-md-3">
											<label for="details">Details </label>
											<textarea class="form-control" rows="1" id="details" name="details">{{ old('details') }}</textarea>
										</div>
									    
										<div class="form-group col-md-2">
									      <label for="balance">Balance</label><br/>
									      <label id="balance" name="balance">0</label>
									    </div>
									    <div class="form-group col-md-2">
									      <label for="amount">Amount</label>
									      <input type="text" class="form-control fill" id="amount" name="amount" value="{{ old('amount')}}" >
									    </div>
									    <div class="form-group col-md-2">
									      <label for="paymode">Payment Mode</label>
											<select id="paymode" class="form-control" name="paymode">
												<option value="">-------Select-------</option>
			                                    <option value="cash" {{ old('paymode') == 'cash' ? 'selected' : '' }}>Cash</option>
			                                    <option value="chaque" {{ old('paymode') == 'chaque' ? 'selected' : '' }}>Chaque</option>
			                                    <option value="banktobank" {{ old('paymode') == 'banktobank' ? 'selected' : '' }}>Bank To Bank</option>
			                                </select>
									    </div>
									</div>
									<div class="form-row">
										
										
										<div class="form-group col-md-12">
											<label for=""> </label>
											<button type="submit" class="form-control btn waves-effect waves-light btn-grd-success btn-out btn-skew"><i class="icofont-money"></i> Withdrawal</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>

				<hr/>
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h5>Withdrawal List</h5>
							</div>
							<div class="card-block">
								<div class="table-responsive">
									<table class="table table-striped table-hover">
										<thead>
											<tr>
												<th>Partner</th>
												<th>Details</th>
												<th>Amount</th>
												<th>Pay Mode</th>
												<th>Date</th>
											</tr>
										</thead>
										<tbody>
											@if($withdraws !=null)
												@foreach($withdraws as $withdraw)
												<tr>
													<td>{{ $withdraw->user->name }}</td>
													<td>{{ $withdraw->details }}</td>
													<td>{{ $withdraw->amount }}</td>
													<td>{{ $withdraw->type }}</td>
													<td>{{ $withdraw->created_at->format('d-F-Y') }}</td>
												</tr>
												@endforeach
											@endif
										</tbody>
									</table>
									{{ $withdraws->links() }}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).on('change','#partner',function(){
		var userid= document.getElementById("partner").value;
		$.ajax({
			type:'GET',
			url:'/getbalance/'+userid,
			dataType:'json',
			success:function(data){
				document.getElementById('balance').innerText=data;
			},
			error:function(error)
			{
				document.getElementById('balance').innerText=0;
				
			}
		});
	});
</script>
@endsection