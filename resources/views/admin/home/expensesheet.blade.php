@extends('admin.master')

@section('Content-Title')
Expense Sheet
@endsection

@section('Content-Heading')

<i class="icofont-document-folder bg-c-blue"></i>
<div class="d-inline">
	<h5>Expense Sheet</h5>
</div>
@endsection

@section('Main-Content')

<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-block">
				<div class="row p-b-20">
					<div class="col-md-2">
						<a href="{{ URL::to('report/expensethismonth')}}"><button class="btn btn-success">PDF Current Month Report</button></a>
					</div>

					<div class="col-md-4">
						<form method="get" action="{{route('expensereportmonthly')}}">
							<div class="form-group row">
								<div class="col-sm-4">
									<select name="month" class="form-control">
										<option value="01">January</option>
										<option value="02">February</option>
										<option value="03">March</option>
										<option value="04">April</option>
										<option value="05">May</option>
										<option value="06">June</option>
										<option value="07">July</option>
										<option value="08">August</option>
										<option value="09">September</option>
										<option value="10">October</option>
										<option value="11">November</option>
										<option value="12">December</option>
									</select>
								</div>
								<div class="col-sm-4">
									<select name="year" class="form-control">
										<option value="2019">2019</option>
										<option value="2020">2020</option>
										<option value="2021">2021</option>
										<option value="2022">2022</option>
									</select>
								</div>
								<div class="col-sm-4">
									<button class="btn btn-primary">PDF Monthly Report</button>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="row">
					<div class="col-md-10">
						<div class="table-responsive">
							<table class="table table-striped table-hover">
								<thead>
									<tr>
									<th>Expense Type</th>
									<th>Details</th>
									<th>Invoice</th>
									<th>Amount</th>
									<th>Payment Type</th>
									<th>Date</th>
									</tr>
								</thead>
								<tbody>
									@if($expenses !=null)
										@foreach($expenses as $expense)
											<tr>
											<td>{{ $expense->expensestype }}</td>
											<td>{{ $expense->details }}</td>
											<td>{{ $expense->invoice }}</td>
											<td>{{ $expense->amount }}</td>
											<td>{{ $expense->paymode }}</td>
											<td>{{ $expense->created_at->format('d-m-Y') }}</td>
											</tr>
										@endforeach
									@endif
								</tbody>
							</table>
							{{ $expenses->links() }}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



@endsection