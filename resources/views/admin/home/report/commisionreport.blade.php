@extends('admin.master')

@section('Content-Title')
Report : Commissionbase
@endsection

@section('Content-Heading')

<i class="icofont-money bg-c-blue"></i>
<div class="d-inline">
	<h5>Report : Commissionbase</h5>
</div>
@endsection

@section('Main-Content')

<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-block">
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h5>Report : Commissionbase</h5>
							</div>
							<div class="card-block">
								<div class="table-responsive">
									<table class="table">
										<thead>
											<tr>
												<th>Company</th>
												<th>Invoice</th>
												<th>Amount</th>
												<th>Status</th>
												<th>Pay Mode</th>
												<th>Date</th>
											</tr>
										</thead>
										<tbody>
											@if($commissions !=null)
												@foreach($commissions as $commission)
												<tr>
													<td>{{ $commission->company }}</td>
													<td>{{ $commission->invoice }}</td>
													<td>
                                                        {{ $commission->amount }}
                                                    </td>
                                                    <td>
                                                            @if($commission->status =='paid')
                                                                <label class="text-success"> {{ $commission->status }}</label>
                                                                @else
                                                                <label class="text-danger"> {{ $commission->status }}</label>
                                                            @endif
                                                    </td>
													<td>
                                                            @if($commission->paymode =='banktobank')
                                                            <label class="text-danger"> Bank To Bank</label>
                                                            @elseif($commission->paymode =='cash')
                                                            <label class="text-success"> {{ $commission->paymode }}</label>
                                                            @else
                                                            <label class="text-info"> {{ $commission->paymode }}</label>
                                                            @endif
                                                    </td>
													<td>{{ $commission->created_at->format('d-M-Y') }}</td>
												</tr>
												@endforeach
											@endif
										</tbody>
                                    </table>
                                    {{ $commissions->links() }}

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection