@extends('admin.master')

@section('Content-Title')
Report : Office Rent
@endsection

@section('Content-Heading')

<i class="icofont-money bg-c-blue"></i>
<div class="d-inline">
	<h5>Report : Office Rent</h5>
</div>
@endsection

@section('Main-Content')

<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-block">
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h5>Report : Office Rent</h5>
							</div>
							<div class="card-block">
								<div class="table-responsive">
									<table class="table">
										<thead>
											<tr>
												<th>Details</th>
												<th>Amount</th>
												<th>Status</th>
												<th>Pay Mode</th>
												<th>Date</th>
											</tr>
										</thead>
										<tbody>
											@if($rents !=null)
												@foreach($rents as $rent)
												<tr>
													<td>{{ $rent->details }}</td>
													<td>
                                                        {{ $rent->amount }}
                                                    </td>
                                                    <td>
                                                            @if($rent->status =='paid')
                                                                <label class="text-success"> {{ $rent->status }}</label>
                                                                @else
                                                                <label class="text-danger"> {{ $rent->status }}</label>
                                                            @endif
                                                    </td>
													<td>
                                                            @if($rent->paymode =='banktobank')
                                                            <label class="text-danger"> Bank To Bank</label>
                                                            @elseif($rent->paymode =='cash')
                                                            <label class="text-success"> {{ $rent->paymode }}</label>
                                                            @else
                                                            <label class="text-info"> {{ $rent->paymode }}</label>
                                                            @endif
                                                    </td>
													<td>{{ $rent->created_at->format('d-M-Y') }}</td>
												</tr>
												@endforeach
											@endif
										</tbody>
                                    </table>
                                    {{ $rents->links() }}

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection