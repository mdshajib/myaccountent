@extends('admin.master')

@section('Content-Title')
Report : Office Accesories
@endsection

@section('Content-Heading')

<i class="icofont-money bg-c-blue"></i>
<div class="d-inline">
	<h5>Report : Office Accesories</h5>
</div>
@endsection

@section('Main-Content')

<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-block">
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h5>Report : Office Accesories</h5>
							</div>
							<div class="card-block">
								<div class="table-responsive">
									<table class="table">
										<thead>
											<tr>
												<th>Items</th>
												<th>Amount</th>
												<th>Status</th>
												<th>Pay Mode</th>
												<th>Date</th>
											</tr>
										</thead>
										<tbody>
											@if($accesoriess !=null)
												@foreach($accesoriess as $accesories)
												<tr>
													<td>{{ $accesories->details }}</td>
													<td>
                                                        {{ $accesories->amount }}
                                                    </td>
                                                    <td>
                                                            @if($accesories->status =='paid')
                                                                <label class="text-success"> {{ $accesories->status }}</label>
                                                                @else
                                                                <label class="text-danger"> {{ $accesories->status }}</label>
                                                            @endif
                                                    </td>
													<td>
                                                            @if($accesories->paymode =='banktobank')
                                                            <label class="text-danger"> Bank To Bank</label>
                                                            @elseif($accesories->paymode =='cash')
                                                            <label class="text-success"> {{ $accesories->paymode }}</label>
                                                            @else
                                                            <label class="text-info"> {{ $accesories->paymode }}</label>
                                                            @endif
                                                    </td>
													<td>{{ $accesories->created_at->format('d-M-Y') }}</td>
												</tr>
												@endforeach
											@endif
										</tbody>
                                    </table>
                                    {{ $accesoriess->links() }}

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection