@extends('admin.master')

@section('Content-Title')
All Quotation
@endsection

@section('Content-Heading')

<i class="icofont-money bg-c-blue"></i>
<div class="d-inline">
	<h5>All Quotation</h5>
</div>
@endsection

@section('Main-Content')

<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-block">
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h5>All Quotation</h5>
							</div>
							<div class="card-block">
								<div class="table-responsive">
									<table class="table">
										<thead>
											<tr>
												<th>Customer</th>
												<th>Address</th>
												<th>Quotation</th>
												<th>Amount</th>
												<th>Date</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											@if($quotations !=null)
												@foreach($quotations as $quotation)
												<tr>
													<td>{{ $quotation->customername }}</td>
													<td>{{ $quotation->address }}</td>
													<td>{{ $quotation->quotation }}</td>
													<td>{{ $quotation->grandtotal }}</td>
													<td>{{ $quotation->created_at->format('d-M-Y') }}</td>
													<td>
														<a class="btn btn-out btn-danger btn-mini btn-square" href="">Order Now</a>
														<a class="btn btn-out waves-effect waves-light hor-grd btn-grd-success btn-mini btn-square" href="{{ URL::to('quotation/'.encrypt($quotation->id)) }}" title="View sale Quotation">View</a>
													</td>
												</tr>
												@endforeach
											@endif
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection