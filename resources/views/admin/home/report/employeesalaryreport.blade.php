@extends('admin.master')

@section('Content-Title')
Report : Consultenct Fee
@endsection

@section('Content-Heading')

<i class="icofont-money bg-c-blue"></i>
<div class="d-inline">
	<h5>Report : Consultenct Fee</h5>
</div>
@endsection

@section('Main-Content')

<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-block">
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h5>Report : Consultenct Fee</h5>
							</div>
							<div class="card-block">
								<div class="table-responsive">
									<table class="table">
										<thead>
											<tr>
												<th>Company</th>
												<th>Invoice</th>
												<th>Amount</th>
												<th>Status</th>
												<th>Pay Mode</th>
												<th>Date</th>
											</tr>
										</thead>
										<tbody>
											@if($consultencys !=null)
												@foreach($consultencys as $consultency)
												<tr>
													<td>{{ $consultency->company }}</td>
													<td>{{ $consultency->invoice }}</td>
													<td>
                                                        {{ $consultency->amount }}
                                                    </td>
                                                    <td>
                                                            @if($consultency->status =='paid')
                                                                <label class="text-success"> {{ $consultency->status }}</label>
                                                                @else
                                                                <label class="text-danger"> {{ $consultency->status }}</label>
                                                            @endif
                                                    </td>
													<td>
                                                            @if($consultency->paymode =='banktobank')
                                                            <label class="text-danger"> Bank To Bank</label>
                                                            @elseif($consultency->paymode =='cash')
                                                            <label class="text-success"> {{ $consultency->paymode }}</label>
                                                            @else
                                                            <label class="text-info"> {{ $consultency->paymode }}</label>
                                                            @endif
                                                    </td>
													<td>{{ $consultency->created_at->format('d-M-Y') }}</td>
												</tr>
												@endforeach
											@endif
										</tbody>
                                    </table>
                                    {{ $consultencys->links() }}

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection