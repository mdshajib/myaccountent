@extends('admin.master')

@section('Content-Title')
Report : Customer Gift
@endsection

@section('Content-Heading')

<i class="icofont-money bg-c-blue"></i>
<div class="d-inline">
	<h5>Report : Customer Gift</h5>
</div>
@endsection

@section('Main-Content')

<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-block">
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h5>Report : Customer Gift</h5>
							</div>
							<div class="card-block">
								<div class="table-responsive">
									<table class="table">
										<thead>
											<tr>
												<th>Gift</th>
												<th>Amount</th>
												<th>Status</th>
												<th>Pay Mode</th>
												<th>Date</th>
											</tr>
										</thead>
										<tbody>
											@if($gifts !=null)
												@foreach($gifts as $gift)
												<tr>
													<td>{{ $gift->details }}</td>
													<td>
                                                        {{ $gift->amount }}
                                                    </td>
                                                    <td>
                                                            @if($gift->status =='paid')
                                                                <label class="text-success"> {{ $gift->status }}</label>
                                                                @else
                                                                <label class="text-danger"> {{ $gift->status }}</label>
                                                            @endif
                                                    </td>
													<td>
                                                            @if($gift->paymode =='banktobank')
                                                            <label class="text-danger"> Bank To Bank</label>
                                                            @elseif($gift->paymode =='cash')
                                                            <label class="text-success"> {{ $gift->paymode }}</label>
                                                            @else
                                                            <label class="text-info"> {{ $gift->paymode }}</label>
                                                            @endif
                                                    </td>
													<td>{{ $gift->created_at->format('d-M-Y') }}</td>
												</tr>
												@endforeach
											@endif
										</tbody>
                                    </table>
                                    {{ $gifts->links() }}

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection