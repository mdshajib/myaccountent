@extends('admin.master')

@section('Content-Title')
Home
@endsection

@section('Content-Heading')

<i class="icofont-ui-home bg-c-blue"></i>
<div class="d-inline">
	<h5>Home</h5>
</div>
@endsection

@section('Main-Content')

<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-block">
				<div class="row">
					<div class="col-md-3">
						<div class="p-0 z-depth-bottom-0">
							<div class="card text-white card-danger">
								<div class="card-header"><h4><i class="icofont-coins"></i> Expense </h4></div>
								<div class="card-body">
									<h5 class="card-title">Total expense of this month</h5>
									<h2 class="card-text"><i class="icofont-taka-true"></i> {{ $expense->expense }}.</h2>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-3">
						<div class="p-0 z-depth-bottom-0">
							<div class="card text-white card-success">
								<div class="card-header"><h4><i class="icofont-money"></i>  Income </h4></div>
								<div class="card-body">
									<h5 class="card-title">Total income of this month</h5>
									<h2 class="card-text"><i class="icofont-taka-true"></i> {{ $income->income }}.</h2>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-3">
						<div class="p-0 z-depth-bottom-0">
							<div class="card text-white card-primary">
								<div class="card-header"><h4><i class="icofont-coins"></i> Profit </h4></div>
								<div class="card-body">
									<h5 class="card-title">Total profit of this month</h5>
									<h2 class="card-text"><i class="icofont-taka-true"></i></i> {{ $profit }}.</h2>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-3">
						<div class="p-0 z-depth-bottom-0">
							<div class="card text-white card-warning">
								<div class="card-header"><h4><i class="icofont-woman-in-glasses"></i> Single Partner</h4></div>
								<div class="card-body">
									<h5 class="card-title">Partner profit of this month</h5>
									<h2 class="card-text"><i class="icofont-taka-true"></i>{{ $partner }}.</h2>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="card">
							<div class="card-header">
								<h5>Last 5 Income</h5>
							</div>
							<div class="card-block">
								<div class="table-responsive">
									<table class="table table-striped table-hover">
										<thead>
											<tr>
												<th>Income Type</th>
												<th>Company</th>
												<th>Amount</th>
												<th>Payment</th>
											</tr>
										</thead>
										<tbody>
											@if($incomesheets !=null)
												@foreach($incomesheets as $incomesheet)
												<tr>
													<td>{{ $incomesheet->incometype }}</td>
													<td>{{ $incomesheet->company }}</td>
													<td>{{ $incomesheet->amount }}</td>
													<td>
														@if($incomesheet->paymenttype =='cash')
														<span class="label label-info">{{$incomesheet->paymenttype}}</span>
														@elseif($incomesheet->paymenttype =='chaque')
														<span class="label label-warning">{{$incomesheet->paymenttype}}</span>
														@endif

													</td>
												</tr>
												@endforeach
											@endif
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-6">
						<div class="card">
							<div class="card-header">
								<h5>Last 5 Expense</h5>
							</div>
							<div class="card-block">
								<div class="table-responsive">
									<table class="table">
										<thead>
											<tr>
												<th>Expense Type</th>
												<th>Details</th>
												<th>Amount</th>
												<th>Payment</th>
											</tr>
										</thead>
										<tbody>
											@if($expensesheets !=null)
												@foreach($expensesheets as $expensesheet)
												<tr>
													<td>{{ $expensesheet->expensestype }}</td>
													<td>{{ $expensesheet->details }}</td>
													<td>{{ $expensesheet->amount }}</td>
													<td>
														@if($expensesheet->paymenttype =='cash')
														<span class="label label-info">{{$expensesheet->paymenttype}}</span>
														@elseif($expensesheet->paymenttype =='chaque')
														<span class="label label-warning">{{$expensesheet->paymenttype}}</span>
														@endif
													</td>
												</tr>
												@endforeach
											@endif
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="card">
							<div class="card-header">
								<h5>Last 3 month</h5>
							</div>
							<div class="card-block">
								<div class="table-responsive">
									<table class="table">
										<thead>
											<tr>
												<th>Transaction ID</th>
												<th>Income</th>
												<th>Expense</th>
												<th>Profit</th>
												<th>Partner</th>
												<th>Month</th>
											</tr>
										</thead>
										<tbody>
											@if($transactions !=null)
												@foreach($transactions as $transaction)
												<tr>
													<td>{{ $transaction->trxid }}</td>
													<td>{{ $transaction->income }}</td>
													<td>{{ $transaction->expense }}</td>
													<td>
														@if($transaction->profit>0)
														<span class="label label-primary">{{$transaction->profit }}</span>
														@else
														<span class="label label-danger">{{$transaction->profit }}</span>
														@endif

													</td>
													<td>{{ $transaction->eachpartner }}</td>
													<td>{{ $transaction->created_at->format('d-m-Y') }}</td>
												</tr>
												@endforeach
											@endif
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">

					</div>
				</div>

			</div>
		</div>
	</div>
</div>


@endsection