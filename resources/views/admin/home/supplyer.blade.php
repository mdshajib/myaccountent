@extends('admin.master')

@section('Content-Title')
Supplyer
@endsection

@section('Content-Heading')

<i class="icofont-money bg-c-blue"></i>
<div class="d-inline">
	<h5>Supplyer</h5>
</div>
@endsection

@section('Main-Content')

<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-block">
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h5>Add New Supplyer</h5>
							</div>
							<div class="card-block">
								@if($message = Session::get('error'))
									<div class="alert alert-danger alert-block">
										<button type="button" class="close" data-dismiss="alert">×</button>	
									        <strong>{{ $message }}</strong>
									</div>

									@elseif($message = Session::get('success'))
									<div class="alert alert-success alert-block">
										<button type="button" class="close" data-dismiss="alert">×</button>	
									        <strong>{{ $message }}</strong>
									</div>

								@endif

								@if ($errors->any())
								    <div class="alert alert-danger">
								        <ul>
								            @foreach ($errors->all() as $error)
								                <p>{{ $error }}</p>
								            @endforeach
								        </ul>
								    </div>
								@endif
								<form id="supplyer" action="{{route('supplyer')}}" method="post">
									@csrf
									<div class="form-row">
									    <div class="form-group col-md-4">
									      <label for="company">Company name </label>
									      <input type="text" class="form-control" id="company" name="company" value="{{ old('company') }}">
									    </div>

									    <div class="form-group col-md-4">
									      <label for="email">Email</label>
									      <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}">
									    </div>

									    <div class="form-group col-md-4">
									      <label for="phone">Phone</label>
									      <input type="text" class="form-control" id="phone" name="phone" value="{{ old('phone') }}">
									    </div>


									</div>
									<div class="form-row">
										

										<div class="form-group col-md-4">
											<label for="address">Address </label>
											<input type="text" class="form-control" id="address" name="address" value="{{ old('address') }}" >
										</div>
										<div class="form-group col-md-4">
											<label for="website">Website </label>
											<input type="text" class="form-control" id="website" name="website" value="{{ old('website') }}" >
										</div>



										<div class="form-group col-md-4">
											<label for=""> </label>
											<button type="submit" class="form-control btn waves-effect waves-light hor-grd btn-grd-info btn-out btn-skew"><i class="icofont-money"></i> Add New Supplyer</button>
										</div>


									</div>
								</form>
							</div>
						</div>
					</div>
				</div>

				<hr/>
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h5>All Supplyer Company</h5>
							</div>
							<div class="card-block">
								<div class="table-responsive">
									<table class="table table-striped table-hover">
										<thead>
											<tr>
												<th>Company</th>
												<th>Email</th>
												<th>Phone</th>
												<th>Address</th>
												<th>website</th>
											</tr>
										</thead>
										<tbody>
											@if($companys !=null)
												@foreach($companys as $company)
												<tr>
													<td>{{ $company->name }}</td>
													<td>{{ $company->email }}</td>
													<td>{{ $company->contacts}}</td>
													<td>{{ $company->address}}</td>
													<td>{{ $company->website}}</td>
												</tr>
												@endforeach
											@endif
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection