@extends('admin.master')

@section('Content-Title')
Consultency Fee
@endsection

@section('Content-Heading')

<i class="icofont-money bg-c-blue"></i>
<div class="d-inline">
	<h5>Consultency Fee</h5>
</div>
@endsection

@section('Main-Content')


<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-block">
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-block">
								@if($message = Session::get('error'))
									<div class="alert alert-danger alert-block">
										<button type="button" class="close" data-dismiss="alert">×</button>	
									        <strong>{{ $message }}</strong>
									</div>

									@elseif($message = Session::get('success'))
									<div class="alert alert-success alert-block">
										<button type="button" class="close" data-dismiss="alert">×</button>	
									        <strong>{{ $message }}</strong>
									</div>

								@endif

								@if ($errors->any())
								    <div class="alert alert-danger">
								        <ul>
								            @foreach ($errors->all() as $error)
								                <p>{{ $error }}</p>
								            @endforeach
								        </ul>
								    </div>
								@endif
								<form id="second" method="post">
									@csrf

									<div class="form-row">

									    <div class="form-group col-md-4">
									    	<label for="invoice">invoice</label>
									    	<input type="text" class="form-control fill" id="invoice" name="invoice" value="{{ old('invoice') }}">
									    </div>

									    <div class="form-group col-md-4">
									    	<label for="amount">Amount</label>
									    	<input type="text" class="form-control fill" id="amount" name="amount" value="{{ old('amount') }}">
									    </div>

									    <div class="form-group col-md-4">
									    	<label for="paymode">Payment Mode</label>
											<select id="paymode" class="form-control" name="paymode">
												<option value="">-------Select-------</option>
			                                    <option value="cash" {{ old('paymode') == 'cash' ? 'selected' : '' }}>Cash</option>
			                                    <option value="chaque" {{ old('paymode') == 'chaque' ? 'selected' : '' }}>Chaque</option>
			                                    <option value="banktobank" {{ old('paymode') == 'banktobank' ? 'selected' : '' }}>Bank To Bank</option>
			                                </select>
									    </div>

									</div>

									<div class="form-row">
									    <div class="form-group col-md-3">
									    	<label for="company">Company </label>
									    	<input type="text" class="form-control fill" id="company" name="company">
									    </div>

										<div class="form-group col-md-3">
									    	<label for="address">Address </label>
											<textarea class="form-control" rows="1" id="address" name="address">{{ old('address') }}</textarea>
									    </div>
									    <div class="form-group col-md-3">
											 <label for="status">Payment Status</label>
											<select id="status" class="form-control" name="status">
			                                    <option value="">-------Select-------</option>
			                                    <option value="paid" {{ old('status') == 'paid' ? 'selected' : '' }}>Paid</option>
			                                    <option value="unpaid" {{ old('status') == 'unpaid' ? 'selected' : '' }}>Unpaid</option>
			                                    
			                                </select>
										</div>
										<div class="form-group col-md-3">
											<label for=""> </label>
											<button type="submit" class="form-control btn waves-effect waves-light btn-grd-success btn-out btn-skew"><i class="icofont-money"></i> Submit Consultency Fee</button>
										</div>

									</div>
								</form>
							</div>
						</div>

					</div>
				</div>

				<hr/>
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h5>Unpaid Consuntency Fee</h5>
							</div>
							<div class="card-block">
								<div class="table-responsive">
									<table class="table table-striped table-hover">
										<thead>
											<tr>
												<th>Company</th>
												<th>Invoice</th>
												<th>Amount</th>
												<th>Pay Mode</th>
												<th>Date</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											@if($unpaids !=null)
												@foreach($unpaids as $unpaid)
												<tr>
													<td>{{ $unpaid->company }}</td>
													<td>{{ $unpaid->invoice }}</td>
													<td>{{ $unpaid->amount }}</td>
													<td>
														@if($unpaid->paymode =='banktobank')
														<label class="text-danger"> Bank To Bank</label>

														@elseif($unpaid->paymode =='cash')
														<label class="text-success"> {{ $unpaid->paymode }}</label>

														@else
														<label class="text-info"> {{ $unpaid->paymode }}</label>
														
														@endif
													</td>
													<td>{{ $unpaid->created_at->format('d-M-Y') }}</td>
													<td><a class="btn btn-out btn-danger btn-mini btn-square" href="{{ URL::to('paidincomes/'.encrypt($unpaid->id))}}">Paid Now</a></td>
													
												</tr>
												@endforeach
											@endif
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>
@endsection