<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style type="text/css">
      body{
        height: auto;
        width: 700px;
      }
      .header-area{
        height: 75px;
        width: 700px;
      }
      .logo{
        height: 70px;
        width: 80px;
        float: left;
      }
      .header-text{
        height: 70px;
        width: 620px;
        float: left;
        text-align: center;
      }
      .company-name{
        font-weight: bold;
        font-size: 35px;
        text-decoration: underline;
      }
      .description{
        font-size: 16px;
        font-weight: bold;
      }
      .address-box{
        height: 90px;
        width: 700px;
      }
      .address1{
        height: 90px;
        width: 300px;
        float: left;
        text-align: left;
      }
      
      .address2{
        height: 90px;
        width: 250px;
        float: right;
        text-align: right;
      }
      .address1>p, .address2>p{
        line-height: 5px;
      }

      .type{
        width: 700px;
        height: 40px;
        text-align: center;
      }
      .invoiceno-box{
        width: 700px;
        height: 80px;
      }
      .left-box{
        height: 80px;
        width: 300px;
        float: left;
      }
      .right-box{
        height: 80px;
        width: 300px;
        float: right;
        text-align: right;
      }
      .receiver-address{
        width: 700px;
        height: 95px;
      }
      .data-table{
        width: 700px;
        height: auto;
      }
      table{
        width: 100%;
        border-collapse: collapse;
        border:1px solid #000;

      }

      table>thead>tr>th{
        border: 1px solid #000;
      }
      table>tbody>tr>td{
        border-left: 1px solid #000;
        border-right: 1px solid #000;
      }
      th,td{
        white-space: nowrap;
        padding: 8px;
      }
      table tr{
        min-height: 40px;
      }
      tr th,tr td{
        text-align: center;
      }
      tr.tdborder td{
        border:1px solid #000;
      }
      .amount-inword{
        width: 700px;
        height: 30px;
      }
      .signature-area{
        width: 700px;
        height: 70px;
        padding-top: 30px;
      }
      .receiver-sig{
        width: 200px;
        height: 40px;
        float: left;
      }
      .sender-sig{
        width: 350px;
        height: 40px;
        float: right;
      }




    </style>
</head>
<body>
   <div class="header-area">
      <div class="logo">
        <img src="" alt="Logo">
      </div>

      <div class="header-text">
        <div class="company-name">
          4S ADVANCE TECHNOLOGIES LTD.
        </div>
        <div class="description">
          All kinds of Imported Shoe Materials and Shoe Machine’s Spare Parts Supplier.
        </div>
      </div>
    </div>

    <div class="address-box">
      <div class="address1">
        <p>Registered office Address:</p>
        <p>Ka-80, Mohakhali south zone</p>
        <p>Dhaka -1212, Bangladesh.</p>
      </div>

      <div class="address2">
        <p>Present office Address:</p>
        <p>House # 12/D, Bashbari Road</p>
        <p>Mohammadpur, Dhaka-1207</p>
        <p>E-mail:4stradinghouse@gmail.com</p>
      </div>
    </div>

    <div class="type">
      <span style="background: #000;color: #fff;font-size: 18px;font-weight: bold;padding:20px;">
        BILL
      </span>
    </div>

    <div class="invoiceno-box">
      <div class="left-box">
          <p>Invoice No. _____________ </p>
          <p>Challan No: _____________</p> 
      </div>

      <div class="right-box">
        <p> Date: ___________</p> 
      </div>
    </div>

    <div class="receiver-address">
      <p>To,</p>
      <p>M/S: _____________________________________________________________________________</p>
      <p>Address: _________________________________________________________________________</p>
    </div>

    <div class="data-table">
      <table>
        <thead>
          <tr>
            <th width="10">SL<br/>No.</th>
            <th width="200">Description of Goods</th>
            <th width="50">Quantity</th>
            <th width="40">Unit</th>
            <th width="40">Unit Price</th>
            <th width="50">Total Price</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>Shoe machvf fgfgf fgfg gf 1235</td>
            <td>1</td>
            <td>1</td>
            <td>10000</td>
            <td>10000</td>
          </tr>

          <tr>
            <td>1</td>
            <td>Shoe machine 1235</td>
            <td>1</td>
            <td>1</td>
            <td>10000</td>
            <td>10000</td>
          </tr>

          <tr>
            <td>1</td>
            <td>Shoe machine 1235</td>
            <td>1</td>
            <td>1</td>
            <td>10000</td>
            <td>10000</td>
          </tr>

          <tr>
            <td>1</td>
            <td>Shoe machine 1235</td>
            <td>1</td>
            <td>1</td>
            <td>10000</td>
            <td>10000</td>
          </tr>

          <tr>
            <td>1</td>
            <td>Shoe machine 1235</td>
            <td>1</td>
            <td>1</td>
            <td>10000</td>
            <td>10000</td>
          </tr>

          <tr class="tdborder">
            <td colspan="4"></td>
            <td>Total :</td>
            <td>10000</td>
          </tr>

        </tbody>

      </table>
    </div>

    <div class="amount-inword">
      <p>Total Amount in Words (BDT): ____________________________________</p>
    </div>

    <div class="signature-area">
      <div class="receiver-sig">
        <span>Receiver’s Signature</span>
      </div>

      <div class="sender-sig">
        <span>For 4S Advance Technologies Ltd.</span>
      </div>
    </div>

    



</body>
</html>