<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style type="text/css">
        body{
            height:794px;
            width:1123px;
        }
        .header-area{
        	height:auto;
        	width: auto;
        	margin: auto;
        	float: left;
        }
        .logo{
        	height:100px;
        	width: 100px;
        	float: left;
        }
        .header-text{
        	height: 100px;
        	width: 1023px;
        	font-weight: bold;
        	float: left;
        	text-align: center;
        }
        .company-name{
        	font-size: 35px;
        	text-decoration: underline;
        }
        .description{
        	font-size: 16px;
        }

        .content-area{
        	height:auto;
        	width: auto;
        	margin: auto;
        	float: left;
        }
        .footer-area{
        	height:auto;
        	width: auto;
        	margin: auto;
        	float: left;
        }
    </style>
</head>
<body>
   <div class="header-area">
   		<div class="logo">

   		</div>

   		<div class="header-text">
   			<div class="company-name">
   				4S ADVANCE TECHNOLOGIES LTD.
   			</div>
   			<div class="description">
   				All kinds of Imported Shoe Materials and Shoe Machine’s Spare Parts Supplier.
   			</div>
   		</div>
   	</div>

   	<div class="content-area">

   	</div>

   	<div class="footer-area">

   	</div>


</body>
</html>