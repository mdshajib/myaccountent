@extends('admin.master')

@section('Content-Title')
Users
@endsection

@section('Content-Heading')

<i class="icofont-money bg-c-blue"></i>
<div class="d-inline">
	<h5>Users</h5>
</div>
@endsection

@section('Main-Content')

<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-block">
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h5>Add New User</h5>
							</div>
							<div class="card-block">
								@if($message = Session::get('error'))
									<div class="alert alert-danger alert-block">
										<button type="button" class="close" data-dismiss="alert">×</button>	
									        <strong>{{ $message }}</strong>
									</div>

									@elseif($message = Session::get('success'))
									<div class="alert alert-success alert-block">
										<button type="button" class="close" data-dismiss="alert">×</button>	
									        <strong>{{ $message }}</strong>
									</div>

								@endif

								@if ($errors->any())
								    <div class="alert alert-danger">
								        <ul>
								            @foreach ($errors->all() as $error)
								                <p>{{ $error }}</p>
								            @endforeach
								        </ul>
								    </div>
								@endif
								<form id="user" action="{{route('employee')}}" method="post">
									@csrf
									<div class="form-row">
									    <div class="form-group col-md-3">
									      <label for="fullname">Full name </label>
									      <input type="text" class="form-control" id="fullname" name="fullname" value="{{ old('fullname') }}">
									    </div>

									    <div class="form-group col-md-3">
									      <label for="email">Email</label>
									      <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}">
									    </div>

									    <div class="form-group col-md-3">
									      <label for="phone">Phone</label>
									      <input type="text" class="form-control" id="phone" name="phone" value="{{ old('phone') }}">
									    </div>

									    <div class="form-group col-md-3">
									      <label for="education">Education</label>
									      <input type="text" class="form-control" id="education" name="education" value="{{ old('education') }}">
									    </div>


									</div>
									<div class="form-row">
										

										<div class="form-group col-md-3">
											<label for="designation">Designation </label>
											<input type="text" class="form-control" id="designation" name="designation" value="{{ old('designation') }}">
										</div>

										<div class="form-group col-md-3">
									      <label for="salary">Salary</label>
									      <input type="text" class="form-control" id="salary" name="salary" value="{{ old('salary') }}">
									    </div>

									    <div class="form-group col-md-3">
									      <label for="date">Join Date</label>
									      <input type="text" class="form-control" id="date" name="date" value="{{ old('date') }}">
									    </div>

										<div class="form-group col-md-3">
											<label for=""> </label>
											<button type="submit" class="form-control btn waves-effect waves-light hor-grd btn-grd-info btn-out btn-skew"><i class="icofont-money"></i> Add New Employee</button>
										</div>


									</div>
								</form>
							</div>
						</div>
					</div>
				</div>

				<hr/>
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h5>All Employee</h5>
							</div>
							<div class="card-block">
								<div class="table-responsive">
									<table class="table table-striped table-hover">
										<thead>
											<tr>
												<th>Name</th>
												<th>Email</th>
												<th>Phone</th>
												<th>Designation</th>
												<th>Salary</th>
												<th>Join Date</th>
											</tr>
										</thead>
										<tbody>
											@if($employes !=null)
												@foreach($employes as $employes)
												<tr>
													<td>{{ $employes->name }}</td>
													<td>{{ $employes->email }}</td>
													<td>{{ $employes->phone}}</td>
													<td>
														{{ $employes->designation}}
													</td>
													
													<td>
														{{ $employes->salary}}
													</td>
													<td>
														{{ $employes->joiningdate}}
													</td>
													
												</tr>
												@endforeach
											@endif
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection