<nav class="pcoded-navbar">
	<div class="nav-list">
		<div class="pcoded-inner-navbar main-menu">
			<div class="pcoded-navigation-label"></div>
			<ul class="pcoded-item pcoded-left-item">
				<li class="{{ request()->routeIs('home') ? 'active' : '' }} ">
					<a href="{{route('home')}}" class="waves-effect waves-dark">
						<span class="pcoded-micon">
							<i class="icofont-ui-home"></i>
						</span>
						<span class="pcoded-mtext">Dashboard</span>
					</a>
				</li>


				<li class="pcoded-hasmenu">
					<a href="javascript:void(0)" class="waves-effect waves-dark">
						<span class="pcoded-micon">
							<i class="icofont-money"></i>
						</span>
						<span class="pcoded-mtext">Income</span>
					</a>
					<ul class="pcoded-submenu">
						<li class=" {{ request()->routeIs('commision') ? 'active' : '' }} ">
							<a href="{{route('commision')}}" class="waves-effect waves-dark">
								<span class="pcoded-mtext">Commissiom Base</span>
							</a>
						</li>

						<li class=" {{ request()->routeIs('consultency') ? 'active' : '' }} ">
							<a href="{{route('consultency')}}" class="waves-effect waves-dark">
								<span class="pcoded-mtext">Consuntency Fee</span>
							</a>
						</li>
						
					</ul>
				</li>
				<li class="pcoded-hasmenu">
					<a href="javascript:void(0)" class="waves-effect waves-dark">
						<span class="pcoded-micon">
							<i class="icofont-coins"></i>
						</span>
						<span class="pcoded-mtext">Expenses</span>
					</a>
					<ul class="pcoded-submenu">
						<li class=" ">
							<a href="{{route('officeaccesories')}}" class="waves-effect waves-dark">
								<span class="pcoded-mtext">Office Accesories</span>
							</a>
						</li>

						<li class=" ">
							<a href="{{route('officerent')}}" class="waves-effect waves-dark">
								<span class="pcoded-mtext">Office Rent</span>
							</a>
						</li>

						<li class=" ">
							<a href="{{route('employeesalary')}}" class="waves-effect waves-dark">
								<span class="pcoded-mtext">Employee Salary</span>
							</a>
						</li>

						<li class=" ">
							<a href="{{route('customergift')}}" class="waves-effect waves-dark">
								<span class="pcoded-mtext">Customer Gift</span>
							</a>
						</li>
						
					</ul>
				</li>

				<li class="pcoded-hasmenu">
					<a href="javascript:void(0)" class="waves-effect waves-dark">
						<span class="pcoded-micon">
							<i class="icofont-users-alt-5"></i>
						</span>
						<span class="pcoded-mtext">Sale</span>
					</a>
					<ul class="pcoded-submenu">
						<li class="">
							<a href="{{route('sale')}}" class="waves-effect waves-dark">
								<span class="pcoded-mtext">Sale</span>
							</a>
						</li>

						<li class=" ">
							<a href=" {{ route('duesale') }} " class="waves-effect waves-dark">
								<span class="pcoded-mtext">Paid Sale</span>
							</a>
						</li>
						
					</ul>
				</li>

				<li class="pcoded-hasmenu">
					<a href="javascript:void(0)" class="waves-effect waves-dark">
						<span class="pcoded-micon">
							<i class="icofont-users-alt-5"></i>
						</span>
						<span class="pcoded-mtext">Purchase</span>
					</a>
					<ul class="pcoded-submenu">
						<li class="">
							<a href="{{route('purchase')}}" class="waves-effect waves-dark">
								<span class="pcoded-mtext">Purchase</span>
							</a>
						</li>

						<li class=" ">
							<a href="{{route('duepurchase')}}" class="waves-effect waves-dark">
								<span class="pcoded-mtext">Paid Purchase</span>
							</a>
						</li>
						
					</ul>
				</li>

				<li class="pcoded-hasmenu">
					<a href="javascript:void(0)" class="waves-effect waves-dark">
						<span class="pcoded-micon">
						<i class="icofont-copy-invert"></i>
						</span>
						<span class="pcoded-mtext">Quotation</span>
					</a>
					<ul class="pcoded-submenu">
						<li class="">
							<a href="{{route('quotation')}}" class="waves-effect waves-dark">
								<span class="pcoded-mtext">New Quotation</span>
							</a>
						</li>

						<li class=" ">
							<a href="{{route('allquotation')}}" class="waves-effect waves-dark">
								<span class="pcoded-mtext">All Quotation</span>
							</a>
						</li>
						
					</ul>
				</li>


				<li class="pcoded-hasmenu">
					<a href="javascript:void(0)" class="waves-effect waves-dark">
						<span class="pcoded-micon">
							<i class="icofont-document-folder"></i>
						</span>
						<span class="pcoded-mtext">Report</span>
					</a>
					<ul class="pcoded-submenu">
						<!-- start-->
						<li class="pcoded-hasmenu">
							<a href="javascript:void(0)" class="waves-effect waves-dark">
								<span class="pcoded-micon">
									<i class="icofont-document-folder"></i>
								</span>
								<span class="pcoded-mtext">Incomes</span>
							</a>
							<ul class="pcoded-submenu">
								<li class="">
									<a href="{{URL::to('report/incomesheet')}}" class="waves-effect waves-dark">
										<span class="pcoded-mtext">Income Sheet</span>
									</a>
								</li>

								<li class="">
									<a href="{{URL::to('report/commission')}}" class="waves-effect waves-dark">
										<span class="pcoded-mtext">Commissiom Base</span>
									</a>
								</li>
								<li class="">
									<a href="{{URL::to('report/consultency')}}" class="waves-effect waves-dark">
										<span class="pcoded-mtext">Consuntency Fee</span>
									</a>
								</li>

							</ul>
						</li>


						<!-- end-->

						<!-- start-->
						<li class="pcoded-hasmenu">
							<a href="javascript:void(0)" class="waves-effect waves-dark">
								<span class="pcoded-micon">
									<i class="icofont-document-folder"></i>
								</span>
								<span class="pcoded-mtext">Expenses</span>
							</a>
							<ul class="pcoded-submenu">
								<li class="">
									<a href="{{URL::to('report/expensesheet')}}" class="waves-effect waves-dark">
										<span class="pcoded-mtext">Expense Sheet</span>
									</a>
								</li>
								<li class="">
									<a href="{{URL::to('report/officeaccesories')}}" class="waves-effect waves-dark">
										<span class="pcoded-mtext">Office Accesories</span>
									</a>
								</li>
								<li class="">
									<a href="{{URL::to('report/officerent')}}" class="waves-effect waves-dark">
										<span class="pcoded-mtext">Office Rent</span>
									</a>
								</li>
								<li class="">
									<a href="" class="waves-effect waves-dark">
										<span class="pcoded-mtext">Employee Salary</span>
									</a>
								</li>

								<li class="">
									<a href="{{URL::to('report/customergift')}}" class="waves-effect waves-dark">
										<span class="pcoded-mtext">Customer Gift</span>
									</a>
								</li>

							</ul>
						</li>
						<!-- end-->

						<li class="  ">
							<a href="{{URL::to('report/salary')}}" class="waves-effect waves-dark">
								<span class="pcoded-mtext">Salary Report</span>
							</a>
						</li>
						
					</ul>
				</li>

				<!-- settings start-->

				<li class="pcoded-hasmenu">
					<a href="javascript:void(0)" class="waves-effect waves-dark">
						<span class="pcoded-micon">
						<i class="icofont-gears"></i>
						</span>
						<span class="pcoded-mtext">Setting</span>
					</a>
					<ul class="pcoded-submenu">
						<li class="">
							<a href="{{route('category')}}" class="waves-effect waves-dark">
								<span class="pcoded-mtext">Category</span>
							</a>
						</li>
						<li class=" ">
							<a href="{{route('brand')}}" class="waves-effect waves-dark">
								<span class="pcoded-mtext">Brand</span>
							</a>
						</li>
						<li class=" ">
							<a href="{{route('model')}}" class="waves-effect waves-dark">
								<span class="pcoded-mtext">Model</span>
							</a>
						</li>

						<li class=" ">
							<a href="{{route('product')}}" class="waves-effect waves-dark">
								<span class="pcoded-mtext">Product</span>
							</a>
						</li>

						<li class=" ">
							<a href="{{route('stock')}}" class="waves-effect waves-dark">
								<span class="pcoded-mtext">Stock</span>
							</a>
						</li>

						<li class=" ">
							<a href="{{route('employee')}}" class="waves-effect waves-dark">
								<span class="pcoded-mtext">Employee</span>
							</a>
						</li>

						<li class=" ">
							<a href="{{route('users')}}" class="waves-effect waves-dark">
								<span class="pcoded-mtext">Users</span>
							</a>
						</li>

						<li class=" ">
							<a href="{{route('supplyer')}}" class="waves-effect waves-dark">
								<span class="pcoded-mtext">Suppluyer</span>
							</a>
						</li>
						<li class=" ">
							<a href="{{route('withdrawal')}}" class="waves-effect waves-dark">
								<span class="pcoded-mtext">Withdrawal</span>
							</a>
						</li>

						
					</ul>
				</li>

				<!-- Settings end-->
				
				<li>
					<a href="#" class="waves-effect waves-dark">
						
						 <button id="close" style="background: linear-gradient(to right, #ff0000 0%, #cc00cc 100%); color:#fff; width:50%;width: 95%;height: 35px">Monthly Calculation is over</button>
					</a>
					<script type="text/javascript" src="{{asset('Backend')}}/bower_components/jquery/js/jquery.min.js"></script>

						<script type="text/javascript">
							$("#close").click(function(){
							Swal.fire({
							  title: 'Are you sure? Close monthly calculation?',
							  text: "You won't be able to revert this!",
							  type: 'warning',
							  showCancelButton: true,
							  confirmButtonColor: '#d33',
							  cancelButtonColor: '#3085d6',
							  confirmButtonText: 'Yes, Close Calculation!',
							  backdrop: `
							    rgba(0,0,123,0.4)
								`
							}).then((result) => {
							  if (result.value) {
							    window.location.href = "{{route('closecal')}}";
							  }
							});
							});


						</script>
				</li>
			</ul>
		</div>
	</div>
</nav>
