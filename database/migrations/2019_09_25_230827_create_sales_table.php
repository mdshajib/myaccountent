<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('invoice', 100)->unique();
            $table->decimal('grandtotal',12,2)->default(0.00);
            $table->string('customername', 100);
            $table->string('address', 100);
            $table->enum('status',['paid','unpaid'])->default('unpaid');
            $table->enum('paymode',['cash','chaque','banktobank'])->default('cash');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
}
