<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpensesheetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expensesheets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('expensestype');
            $table->string('details')->default(' ');
            $table->string('invoice')->default(' ');
            $table->decimal('amount',12,2)->default(0.00);
            $table->enum('status',['paid','unpaid'])->default('unpaid');
            $table->enum('paymode',['cash','chaque','banktobank']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expensesheets');
    }
}
